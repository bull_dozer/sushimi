//
//  conta.m
//  Sophia
//
//  Created by MacCVW on 17/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "conta.h"
#import "item.h"
#import "request.h"

NSMutableArray *pedidos;
NSString *mesa, *transacao;

@implementation conta

+ (void)iniciaConta
{
    if (!pedidos)
        pedidos = [[NSMutableArray alloc] initWithCapacity:0];
}

+ (void)inserePedido:(NSArray*)p
{
    [self iniciaConta];
    [pedidos addObject:p];
}

+ (NSArray*)conta
{
    [self iniciaConta];
    return [NSArray arrayWithArray:pedidos];
}

+ (float)totalConta
{
    [self iniciaConta];
    
    double soma = 0;
    
    /*
    for (NSArray *arrayPedido in pedidos)
        for (NSDictionary *dictItem in arrayPedido)
        {
            item *it = [dictItem valueForKey:@"item"];
            int quantidade = [[dictItem valueForKey:@"quantidade"] intValue];
            soma += (quantidade * it.preco);
        }
     */
    
    for (NSDictionary *dictItem in pedidos)
    {
        item *it = [dictItem valueForKey:@"item"];
        int quantidade = [[dictItem valueForKey:@"quantidade"] intValue];
        soma += (quantidade * it.preco);
    }
    
    return soma;
}

+ (void)zeraConta
{
    [self iniciaConta];
    [pedidos removeAllObjects];
    mesa = @"";
    transacao = @"";
}

#pragma mark - Mesa

+ (NSString*)setMesa:(NSString*)m verifica:(BOOL)v
{
    if (!v)
        return @"ok"; //nao verifica, a conta ja foi restaurada
    
    NSString *strUrl = [NSString stringWithFormat:@"pedidos/abreMesa/%@/%@", m, [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSString *retorno = [[NSString alloc] initWithData:d encoding:NSISOLatin1StringEncoding];
    
    //NSLog(@"%@", url.absoluteString);
    //NSLog(@"%@", retorno);
    
    NSArray *arrayRetorno = [retorno componentsSeparatedByString:@"|"];
    if ([[arrayRetorno objectAtIndex:0] isEqualToString:@"ok"])
    {
        mesa = m;
        transacao = [arrayRetorno objectAtIndex:1];
        return @"ok";
    }
    
    else if ([[arrayRetorno objectAtIndex:0] isEqualToString:@"erro"])
    {
        NSString *descErro = [arrayRetorno objectAtIndex:1];
        
        if ([descErro isEqualToString:@"mesa_aberta"])
        {
            //return @"erroa"; //erro aberta
            mesa = m;
            transacao = [arrayRetorno objectAtIndex:2];
            return @"ok";
        }
        
        else if ([descErro isEqualToString:@"mesa_inexistente"])
            return @"erroi"; //erro inexistente
    }
    
    return @"erroc"; //erro conexao
}

+ (NSString*)mesa
{
    if (mesa)
        return mesa;
    return @"";
}

+ (NSString*)transacao
{
    if (transacao)
        return transacao;
    return @"";
}

#pragma mark - Consistencia

+ (NSArray*)puxaMinhaConta
{
    NSMutableArray *arrPedidos = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *strUrl = [NSString stringWithFormat:@"pedidos/produtos/%@/%@", [self transacao], [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];

    for (NSDictionary *p in [JSON valueForKey:@"p"])
    {
        NSMutableDictionary *dictPedido = [[NSMutableDictionary alloc] initWithCapacity:2];
        
        NSArray *catProdutos = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[DOCSFOLDER stringByAppendingFormat:@"/produtos"] error:nil];
        for (NSString *nomePlist in catProdutos)
        {
            int idcat = [[nomePlist stringByReplacingOccurrencesOfString:@".plist" withString:@""] intValue];
            NSArray *itens = [request localProdutosCategoria:idcat];
            for (item *i in itens)
            {
                if ([[p valueForKey:@"idProduto"] intValue] == i.idProd)
                {
                    [dictPedido setValue:i forKey:@"item"];
                    [dictPedido setValue:[p valueForKey:@"quantidade"] forKey:@"quantidade"];
                    [arrPedidos addObject:dictPedido];
                    break;
                }
            }
        }
    }

    pedidos = arrPedidos;
    return [NSArray arrayWithArray:arrPedidos];
}

+ (BOOL)carregaContaDisco
{
    //NSString *pathConta = [DOCSFOLDER stringByAppendingString:@"/conta_backup.plist"];
    NSString *pathMesa = [DOCSFOLDER stringByAppendingString:@"/mesa_backup.plist"];
    NSString *pathTransacao = [DOCSFOLDER stringByAppendingString:@"/transacao_backup.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:pathTransacao])
    {
        return NO;
    }
    else
    {
        //NSArray *arrayConta = [NSArray arrayWithContentsOfFile:pathConta];
        //pedidos = [[NSMutableArray alloc] initWithArray:arrayConta];
        
        //for (NSArray *arrayPedido in pedidos)
        //    for (NSDictionary *dictItem in arrayPedido)
        //        [dictItem setValue:[NSKeyedUnarchiver unarchiveObjectWithData:[dictItem valueForKey:@"item"]] forKey:@"item"];
        
        mesa = [[NSString alloc] initWithContentsOfFile:pathMesa encoding:NSUTF8StringEncoding error:nil];
        transacao = [[NSString alloc] initWithContentsOfFile:pathTransacao encoding:NSUTF8StringEncoding error:nil];
        
        return YES;
    }
}

+ (void)salvaContaDisco
{
    //NSString *pathConta = [DOCSFOLDER stringByAppendingString:@"/conta_backup.plist"];
    NSString *pathMesa = [DOCSFOLDER stringByAppendingString:@"/mesa_backup.plist"];
    NSString *pathTransacao = [DOCSFOLDER stringByAppendingString:@"/transacao_backup.plist"];

    if (transacao && transacao.length > 0)
    {
        /*
        if (pedidos)
        {
            NSMutableArray *contaEncoded = [[NSMutableArray alloc] initWithCapacity:0];
            for (NSArray *arrayPedido in pedidos)
            {
                NSMutableArray *pedidoEncoded = [[NSMutableArray alloc] initWithCapacity:0];
                for (NSDictionary *dictItem in arrayPedido)
                {
                    NSMutableDictionary *dictItemEncoded = [[NSMutableDictionary alloc] initWithDictionary:dictItem];
                    [dictItemEncoded setValue:[NSKeyedArchiver archivedDataWithRootObject:[dictItem valueForKey:@"item"]] forKey:@"item"];
                    [pedidoEncoded addObject:dictItemEncoded];
                }
                [contaEncoded addObject:pedidoEncoded];
            }
            
            [contaEncoded writeToFile:pathConta atomically:YES];
        }
        */
        
        [mesa writeToFile:pathMesa atomically:YES encoding:NSUTF8StringEncoding error:nil];
        [transacao writeToFile:pathTransacao atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}

+ (void)removeContaDisco
{
    //NSString *pathConta = [DOCSFOLDER stringByAppendingString:@"/conta_backup.plist"];
    NSString *pathMesa = [DOCSFOLDER stringByAppendingString:@"/mesa_backup.plist"];
    NSString *pathTransacao = [DOCSFOLDER stringByAppendingString:@"/transacao_backup.plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathTransacao])
    {
        //[[NSFileManager defaultManager] removeItemAtPath:pathConta error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:pathMesa error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:pathTransacao error:nil];
    }
}

+ (NSString*)solicitaFechamento
{
    NSString *strUrl = [NSString stringWithFormat:@"pedidos/solicitafechamento/%@/%@", [self transacao], [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSString *retorno = [[NSString alloc] initWithData:d encoding:NSISOLatin1StringEncoding];
    return retorno;
}

+ (BOOL)statusMesa
{
    NSString *strUrl = [NSString stringWithFormat:@"pedidos/statusMesa/%@/%@", [self transacao], [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSString *retorno = [[NSString alloc] initWithData:d encoding:NSISOLatin1StringEncoding];
    //NSLog(@"%@", strUrl);
    //NSLog(@"Retorno: %@", retorno);
    if ([retorno isEqualToString:@"2"])
        return YES;
    return NO;
}

@end