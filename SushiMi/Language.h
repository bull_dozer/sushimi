//
//  Language.h
//  Sophia
//
//  Created by MacCVW on 23/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSObject

+ (void)initialize;
+ (void)setLanguage:(NSString *)l;
+ (NSString *)get:(NSString *)key;
+ (NSString *)getLanguage;

@end
