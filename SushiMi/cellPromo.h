//
//  cellPromo.h
//  SushiMi
//
//  Created by MacCVW on 04/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBCardapio, DiegoButton, promocao;

@interface cellPromo : UITableViewCell <UIAlertViewDelegate>

@property (nonatomic,strong) DiegoButton *face;
@property (nonatomic,strong) promocao *p;
@property (nonatomic,strong) UIImageView *imgViewPromo;
@property (nonatomic,strong) IBCardapio *pai;

@end
