//
//  cellItem.m
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "cellItem.h"
#import "item.h"
#import "IBVerPratos.h"
#import "IBCardapio.h"
#import "request.h"
#import "pedido.h"
#import "Language.h"
#import "DiegoButton.h"

@implementation cellItem

# pragma mark - Facebook

- (void)publicarNoFacebookMensagem:(NSString*)msg
{
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [act setFrame:btnFace.frame];
    [btnFace setAlpha:0.5];
    [self addSubview:act];
    [act startAnimating];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    if (msg)
        [param setValue:msg forKey:@"message"];
    [param setValue:@"http://www.sushimi.com.br/" forKey:@"link"];
    [param setValue:aux.urlFoto forKey:@"picture"];
    [param setValue:aux.titulo forKey:@"name"];
    [param setValue:aux.descricao forKey:@"description"];
    
    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:param
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (error)
         {
             NSString *alertText = [NSString stringWithFormat: @"Ocorreu um erro: domain = %@, code = %d", error.domain, error.code];
             [[[UIAlertView alloc] initWithTitle:@"Erro" message:alertText delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil] show];
         }
         else
         {
             //[[[UIAlertView alloc] initWithTitle:@"Publicado!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
         }
         
         [act stopAnimating];
         [act removeFromSuperview];
         [btnFace setAlpha:1];
     }];
}

# pragma mark - AlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)     //inserir recado na publicacao?
    {
        if (buttonIndex == 1)   //sim
        {
            UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Recado" message:nil delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Publicar", nil];
            [a setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [[a textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
            [a setTag:2];
            [a show];
        }
        else                    //nao
        {
            [self publicarNoFacebookMensagem:nil];
        }
    }
    
    else if (alertView.tag == 2) //insere recado na publicacao
    {
        if (buttonIndex == 1)
        {
            [self publicarNoFacebookMensagem:[[alertView textFieldAtIndex:0] text]];
        }
    }
}

#pragma mark - Outros

- (void)rate:(DiegoButton*)sender
{
    if ([sender tag] == 0)
        [self.pai abreRatingItem:self.i1];
    else
        [self.pai abreRatingItem:self.i2];
}

- (void)facebook:(DiegoButton*)sender
{
    if ([sender tag] == 0)
    {
        aux = self.i1;
        btnFace = self.face1;
    }
    else
    {
        aux = self.i2;
        btnFace = self.face2;
    }

    if (FBSession.activeSession.isOpen)
    {
        //[FBSession.activeSession closeAndClearTokenInformation]; //serve pra deslogar, acho
        
        /*
        [FBRequestConnection startForPostStatusUpdate:@"Teste!!!" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
        {
            NSLog(@"%@", error);
            //[self showAlert:message result:result error:error];
            //self.buttonPostStatus.enabled = YES;
        }];
         */
        
        
        /*
        [FBRequestConnection startForUploadPhoto:self.i1.foto completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
        {
            NSLog(@"postou");
            //[self showAlert:@"Photo Post" result:result error:error];
            //self.buttonPostPhoto.enabled = YES;
        }];
         */
        
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Mensagem" message:@"Deseja inserir um recado junto da publicação?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil];
        [a setTag:1];
        [a show];
    }
    else
    {
        FBSession.activeSession = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObject:@"publish_actions"]];
        [[FBSession activeSession] openWithBehavior:FBSessionLoginBehaviorForcingWebView completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             if (error)
             {
             }
         }];
    }
}


/*
- (void)addItem
{
    [pedido insereItem:self.i];
}
*/

/*
- (void)abreDetalhes
{
    [self.pai performSelectorInBackground:@selector(fundoBranco) withObject:nil];
    IBVerPratos *p = [[IBVerPratos alloc] init];
    [p setPai:self.pai];
    [p setI:i];
    [p setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [p setModalPresentationStyle:UIModalPresentationPageSheet];
    [pai presentModalViewController:p animated:YES];
    p.view.superview.frame = CGRectMake(100, 100, 762, 470);
    p.view.superview.center = pai.view.center;
}
*/

- (void)abreZoom:(DiegoButton*)sender
{
    UIImage *fotoZoom = ([sender tag] == 0) ? self.i1.foto : self.i2.foto;
    [self.pai abreZoom:fotoZoom];
}

- (void)acaoInserirPedido:(DiegoButton*)sender
{
    DiegoButton *auxPick;
    if ([sender tag] == 0) //item 1
    {
        aux = self.i1;
        auxPick = self.pick1;
    }
    else
    {
        aux = self.i2;
        auxPick = self.pick2;
    }
    
    if ([pedido itemEstaNoPedido:aux])
    {
        [pedido removeItem:aux];
        [auxPick setImage:[UIImage imageNamed:@"checkoff.png"] forState:UIControlStateNormal];
    }
    else
    {
        [pedido insereItem:aux];
        [auxPick setImage:[UIImage imageNamed:@"checkon.png"] forState:UIControlStateNormal];
    }
}

        
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        //Item 1
        self.fundo1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 169, 284, 115)]; //90
        [self.fundo1 setImage:[request corBotao:CorFundoItem]];
        [self addSubview:self.fundo1];
        
        self.foto1 = [[DiegoButton alloc] initWithFrame:CGRectMake(10, 14, 275, 172)];
        [self.foto1.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.foto1 setTag:0];
        [self.foto1 addTarget:self action:@selector(abreZoom:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.foto1];
        
        self.titulo1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 194, 274, 25)];
        [self.titulo1 setBackgroundColor:[UIColor clearColor]];
        [self.titulo1 setTextColor:[UIColor whiteColor]];
        [self.titulo1 setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self.titulo1 setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:self.titulo1];
        
        self.descricao1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 217, 274, 67)]; //42
        [self.descricao1 setBackgroundColor:[UIColor clearColor]];
        [self.descricao1 setTextColor:[UIColor whiteColor]];
        [self.descricao1 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao1 setNumberOfLines:3]; //2
        [self.descricao1 setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:self.descricao1];
        
        self.btnDescricao1 = [[UIButton alloc] initWithFrame:CGRectMake(10, 217, 274, 67)]; //42
        [self.btnDescricao1 setTag:0];
        [self.btnDescricao1 addTarget:self action:@selector(acaoInserirPedido:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnDescricao1];
        
        self.pick1 = [[DiegoButton alloc] initWithFrame:CGRectMake(272, 173, 25, 25)];
        [self.pick1 setImage:[UIImage imageNamed:@"checkon.png"] forState:UIControlStateNormal];
        [self.pick1 setTag:0];
        [self.pick1 addTarget:self action:@selector(acaoInserirPedido:) forControlEvents:UIControlEventTouchUpInside];
        [self.pick1.layer setBorderWidth:2];
        [self.pick1.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self addSubview:self.pick1];
        
        self.star1 = [[DiegoButton alloc] initWithFrame:CGRectMake(272, 1, 25, 25)];
        [self.star1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.star1 setTag:0];
        [self.star1 addTarget:self action:@selector(rate:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.star1];
        
        self.face1 = [[DiegoButton alloc] initWithFrame:CGRectMake(240, 1, 25, 25)];
        [self.face1 setImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
        [self.face1 setTag:0];
        [self.face1 addTarget:self action:@selector(facebook:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.face1];
        
        //Item 2
        self.fundo2 = [[UIImageView alloc] initWithFrame:CGRectMake(315, 169, 284, 115)];
        [self.fundo2 setImage:[request corBotao:CorFundoItem]];
        [self addSubview:self.fundo2];
        
        self.foto2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 10, 14, 275, 172)];
        [self.foto2.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.foto2 setTag:1];
        [self.foto2 addTarget:self action:@selector(abreZoom:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.foto2];
        
        self.titulo2 = [[UILabel alloc] initWithFrame:CGRectMake(315 + 10, 194, 274, 25)];
        [self.titulo2 setBackgroundColor:[UIColor clearColor]];
        [self.titulo2 setTextColor:[UIColor whiteColor]];
        [self.titulo2 setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self.titulo2 setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:self.titulo2];
        
        self.descricao2 = [[UILabel alloc] initWithFrame:CGRectMake(315 + 10, 217, 274, 67)];
        [self.descricao2 setBackgroundColor:[UIColor clearColor]];
        [self.descricao2 setTextColor:[UIColor whiteColor]];
        [self.descricao2 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao2 setNumberOfLines:3];
        [self.descricao2 setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:self.descricao2];
        
        self.btnDescricao2 = [[UIButton alloc] initWithFrame:CGRectMake(315 + 10, 217, 274, 67)];
        [self.btnDescricao2 setTag:1];
        [self.btnDescricao2 addTarget:self action:@selector(acaoInserirPedido:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnDescricao2];
        
        self.pick2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 272, 173, 25, 25)];
        [self.pick2 setImage:[UIImage imageNamed:@"checkon.png"] forState:UIControlStateNormal];
        [self.pick2 setTag:1];
        [self.pick2 addTarget:self action:@selector(acaoInserirPedido:) forControlEvents:UIControlEventTouchUpInside];
        [self.pick2.layer setBorderWidth:2];
        [self.pick2.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self addSubview:self.pick2];
        
        self.star2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 272, 1, 25, 25)];
        [self.star2 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.star2 setTag:1];
        [self.star2 addTarget:self action:@selector(rate:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.star2];
        
        self.face2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 +  240, 1, 25, 25)];
        [self.face2 setImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
        [self.face2 setTag:1];
        [self.face2 addTarget:self action:@selector(facebook:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.face2];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.star1 setHidden:self.esconderIcons];
    [self.face1 setHidden:self.esconderIcons];
    
    [self.fundo2 setHidden:!self.i2];
    [self.foto2 setHidden:!self.i2];
    [self.pick2 setHidden:!self.i2];
    [self.titulo2 setHidden:!self.i2];
    [self.descricao2 setHidden:!self.i2];
    [self.btnDescricao2 setHidden:!self.i2];
    [self.star2 setHidden:!self.i2 || self.esconderIcons];
    [self.face2 setHidden:!self.i2 || self.esconderIcons];
    
    [self.foto1 setImage:self.i1.thumbnail forState:UIControlStateNormal];
    [self.foto2 setImage:self.i2.thumbnail forState:UIControlStateNormal];
    
    [self.titulo1 setText:[[NSString stringWithFormat:@"%@ _ %.2f", [self.i1 titulo], [self.i1 preco]] stringByReplacingOccurrencesOfString:@"." withString:@","]];
    [self.titulo2 setText:[[NSString stringWithFormat:@"%@ _ %.2f", [self.i2 titulo], [self.i2 preco]] stringByReplacingOccurrencesOfString:@"." withString:@","]];
    
    [self.descricao1 setText:[self.i1 descricao]];
    [self.descricao2 setText:[self.i2 descricao]];
    
    if ([pedido itemEstaNoPedido:self.i1])
        [self.pick1 setImage:[UIImage imageNamed:@"checkon.png"] forState:UIControlStateNormal];
    else
        [self.pick1 setImage:[UIImage imageNamed:@"checkoff.png"] forState:UIControlStateNormal];
    
    if ([pedido itemEstaNoPedido:self.i2])
        [self.pick2 setImage:[UIImage imageNamed:@"checkon.png"] forState:UIControlStateNormal];
    else
        [self.pick2 setImage:[UIImage imageNamed:@"checkoff.png"] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
