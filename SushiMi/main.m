//
//  main.m
//  SushiMi
//
//  Created by MacCVW on 01/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TimerApp.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([TimerApp class]), NSStringFromClass([AppDelegate class]));
    }
}