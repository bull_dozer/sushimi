//
//  cellCategoria.h
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBCardapio;

@interface cellCategoria : UITableViewCell

@property (nonatomic,strong) UIButton *btn;
@property  NSInteger idCat;
@property IBCardapio *pai;
@property BOOL ehVoltar;

- (void)acaoBtn;

@end
