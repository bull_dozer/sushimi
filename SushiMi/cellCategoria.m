//
//  cellCategoria.m
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "cellCategoria.h"
#import "IBCardapio.h"
#import "request.h"

@implementation cellCategoria
@synthesize idCat, pai, ehVoltar;

- (void)acaoBtn
{
    if (!ehVoltar)
    {
        [request selecionaBotaoCategoria:self.btn];
        [pai carregaMenu:idCat nome:[self.btn titleForState:UIControlStateNormal]];
    }
    else
    {
        [pai voltaMenu];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setSelectionStyle:UITableViewCellEditingStyleNone];
        self.btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 230, 54)];
        
        [self.btn setTag:idCat];

        static UIImage *fundoUp, *fundoDown, *fundoSelected;
        if (!fundoUp)
            fundoUp = [request corBotao:CorBotaoUp2];
        if (!fundoDown)
            fundoDown = [request corBotao:CorBotaoDown];
        if (!fundoSelected)
            fundoSelected = [request corBotao:CorBotaoSelecionado];

        [self.btn setBackgroundImage:fundoUp forState:UIControlStateNormal];
        [self.btn setBackgroundImage:fundoDown forState:UIControlStateHighlighted];
        [self.btn setBackgroundImage:fundoSelected forState:UIControlStateSelected];
        
        [self.btn.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
        [self.btn setTitleColor:[request corFontes] forState:UIControlStateNormal];
        [self.btn setTitleEdgeInsets:UIEdgeInsetsMake(7, 0, 0, 0)];
        [self.btn addTarget:self action:@selector(acaoBtn) forControlEvents:UIControlEventTouchUpInside];
        [self.btn.titleLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.btn.layer setBorderWidth:1];
        [self.btn.layer setBorderColor:[[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1] CGColor]];

        [self addSubview:self.btn];
    }
    return self;
}

- (void)layoutSubviews
{
    if (ehVoltar)
    {
        static UIImage *fundoUp, *fundoDown;
        if (!fundoUp)
            fundoUp = [request corBotao:CorBotaoVoltarUp];
        if (!fundoDown)
            fundoDown = [request corBotao:CorBotaoVoltarDown];
        
        [self.btn setBackgroundImage:fundoUp forState:UIControlStateNormal];
        [self.btn setBackgroundImage:fundoDown forState:UIControlStateHighlighted];
    }
    else
    {
        static UIImage *fundoUp, *fundoDown, *fundoSelected;
        if (!fundoUp)
            fundoUp = [request corBotao:CorBotaoUp2];
        if (!fundoDown)
            fundoDown = [request corBotao:CorBotaoDown];
        if (!fundoSelected)
            fundoSelected = [request corBotao:CorBotaoSelecionado];
        
        [self.btn setBackgroundImage:fundoUp forState:UIControlStateNormal];
        [self.btn setBackgroundImage:fundoDown forState:UIControlStateHighlighted];
        [self.btn setBackgroundImage:fundoSelected forState:UIControlStateSelected];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
