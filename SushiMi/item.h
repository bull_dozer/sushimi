//
//  item.h
//  Agraz
//
//  Created by MacCVW on 10/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface item : NSObject

@property (nonatomic) int idProd;
@property (nonatomic,retain) NSString *tituloPt, *tituloEn, *tituloEs, *cod, *imagem, *descricaoPt, *descricaoEn, *descricaoEs, *urlFoto;
@property (nonatomic) float preco;
@property (nonatomic,strong) UIImage *thumb;

- (UIImage*)foto;
- (UIImage*)thumbnail;
- (NSString*)titulo;
- (NSString*)descricao;

@end
