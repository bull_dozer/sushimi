//
//  promocao.h
//  SushiMi
//
//  Created by MacCVW on 10/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface promocao : NSObject

@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) UIImage *imagem;

@end
