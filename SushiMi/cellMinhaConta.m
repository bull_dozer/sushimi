//
//  cellMinhaConta.m
//  Sophia
//
//  Created by MacCVW on 16/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "cellMinhaConta.h"
#import "item.h"
#import "request.h"

@implementation cellMinhaConta
@synthesize p, numPedido;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        pedido = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 22)];
        [pedido setBackgroundColor:[UIColor clearColor]];
        [pedido setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [pedido setShadowOffset:CGSizeMake(0, 1)];
        [pedido setShadowColor:[UIColor blackColor]];
        [pedido setTextColor:[UIColor whiteColor]];
        [self addSubview:pedido];
        
        desenhado = NO;
    }
    return self;
}

- (void)layoutSubviews
{
    [pedido setText:[NSString stringWithFormat:@"Pedido %i", numPedido]];
    
    if (!desenhado)
    {
        double somaPedido = 0;
        
        for (int i = 0; i < [p count]; i++)
        {
            NSDictionary *dictItem = [p objectAtIndex:i];
            item *it = [dictItem valueForKey:@"item"];
            int quantidade = [[dictItem valueForKey:@"quantidade"] intValue];
            somaPedido += (quantidade * it.preco);
            
            UILabel *produto = [[UILabel alloc] initWithFrame:CGRectMake(0, (i+1)*22, 500, 22)];
            [produto setBackgroundColor:[UIColor clearColor]];
            [produto setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
            [produto setShadowOffset:CGSizeMake(0, 1)];
            [produto setShadowColor:[UIColor blackColor]];
            [produto setTextColor:[UIColor whiteColor]];
            [produto setText:[NSString stringWithFormat:@"+ %@", it.titulo]];
            [self addSubview:produto];
        }
        
        dados = [[UILabel alloc] initWithFrame:CGRectMake(400, 10, 500, 15+([p count]*22))];
        [dados setTextAlignment:UITextAlignmentRight];
        [dados setBackgroundColor:[UIColor clearColor]];
        [dados setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [dados setShadowOffset:CGSizeMake(0, 1)];
        [dados setShadowColor:[UIColor blackColor]];
        [dados setTextColor:[UIColor whiteColor]];
        [self addSubview:dados];
        
        [dados setText:[[NSString stringWithFormat:@"%@ %.2f", [request moeda], somaPedido] stringByReplacingOccurrencesOfString:@"." withString:[request separadorMoeda]]];
        
        desenhado = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
