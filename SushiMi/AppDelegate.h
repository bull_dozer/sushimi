//
//  AppDelegate.h
//  Sophia
//
//  Created by MacCVW on 25/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class IBInicial;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IBInicial *viewController;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@property (strong, nonatomic) FBSession *session;

@end
