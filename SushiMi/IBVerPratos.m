//
//  IBVerPratos.m
//  Sophia
//
//  Created by MacCVW on 03/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "IBVerPratos.h"
#import "IBCardapio.h"
#import "item.h"
#import "request.h"
#import "Language.h"

@implementation IBVerPratos
@synthesize pai, i;

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return foto;
}

- (IBAction)fechar:(id)sender
{
    if (!estado)
    {
        [request setModalAberta:NO];
        [self dismissModalViewControllerAnimated:YES];
        [pai fundoBranco];
    }
    else
    {
        [self reduzir];
    }
}

- (void)reduzir
{
    estado = 0;
    [zoom setMaximumZoomScale:1];
    [zoom setZoomScale:1 animated:YES];
    [UIView animateWithDuration:1 animations:^
     {
         [close setAlpha:0];
         [zoom setFrame:CGRectMake(29, 60, 331, 349)];
         [zoom setContentSize:CGSizeMake(331, 349)];
         
     } completion:^(BOOL finished)
     {
         [close setImage:[UIImage imageNamed:@"box_close.png"] forState:UIControlStateNormal];
         [UIView animateWithDuration:1 animations:^
          {
              [titulo setAlpha:1];
              [descricao setAlpha:1];
              [close setAlpha:1];
              [valor setAlpha:1];
              [preco setAlpha:1];
              
          } completion:^(BOOL finished)
          {
              [UIView animateWithDuration:0.4 animations:^
              {
                  [foto setAlpha:0];
                  
              } completion:^(BOOL finished)
              {
                  [foto setContentMode:UIViewContentModeScaleAspectFill];
                  [UIView animateWithDuration:0.4 animations:^
                   {
                       [foto setAlpha:1];
                       [ampliar setAlpha:1];
                       
                   } completion:nil];
              }];
         }];
     }];
}

- (void)ampliar
{
    estado = 1;
    [zoom setMaximumZoomScale:4];
    [UIView animateWithDuration:1 animations:^
    {
        [ampliar setAlpha:0];
        [titulo setAlpha:0];
        [descricao setAlpha:0];
        [close setAlpha:0];
        [valor setAlpha:0];
        [preco setAlpha:0];
        
    } completion:^(BOOL finished)
    {
        [close setImage:[UIImage imageNamed:@"box_shorten.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:1 animations:^
        {
            [zoom setFrame:CGRectMake(15, 15, 732, 440)];
            [close setAlpha:1];
            
        } completion:^(BOOL finished)
        {
            [UIView animateWithDuration:0.4 animations:^
            {
                [foto setAlpha:0];
                
            } completion:^(BOOL finished)
            {
                [foto setContentMode:UIViewContentModeScaleAspectFit];
                [UIView animateWithDuration:0.4 animations:^
                {
                    [foto setAlpha:1];
                    
                } completion:nil];
            }];
        }];
    }];
}

- (void)ajustaPersonalizacao
{
    [descricao setTextColor:[request corFontes]];
    [preco setTextColor:[request corFontes]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [request setModalAberta:YES];

    [self ajustaPersonalizacao];
    
    [titulo setAdjustsFontSizeToFitWidth:YES];
    [titulo setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:25]];
    [titulo setText:i.titulo];
    [descricao setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:18]];
    
    if (i.imagem.length > 0)
    {
        [foto setImage:[UIImage imageWithContentsOfFile:[request pathPara:i.imagem]]];
        ampliar = [[UIButton alloc] initWithFrame:CGRectMake(0, 300, 331, 49)];
        [ampliar setTitle:[Language get:@"AMPLIAR"] forState:UIControlStateNormal];
        [ampliar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:24]];
        [ampliar setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [ampliar setTitleEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];
        [ampliar setBackgroundImage:[UIImage imageNamed:@"rodape_fundo_img_produto.png"] forState:UIControlStateNormal];
        [ampliar addTarget:self action:@selector(ampliar) forControlEvents:UIControlEventTouchUpInside];
        [foto addSubview:ampliar];
    }
    else
    {
        NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
        if ([[p valueForKey:@"logo"] length] > 0)
            [foto setImage:[UIImage imageWithContentsOfFile:[request pathPara:[p valueForKey:@"logo"]]]];
    }
    
    [zoom.layer setCornerRadius:13];
    
    [valor setText:[Language get:@"PRECO"]];
    [valor setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
    [preco setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
    
    [descricao setText:i.descricao];
    CGRect frame = descricao.frame;
    frame.size.height = descricao.contentSize.height;
    if (frame.size.height > 270)
        frame.size.height = 270;
    descricao.frame = frame;
    
    [valor setFrame:CGRectMake(valor.frame.origin.x,
                               frame.size.height + descricao.frame.origin.y + 15,
                               valor.frame.size.width,
                               valor.frame.size.height)];
    
    [preco setFrame:CGRectMake(preco.frame.origin.x,
                               frame.size.height + descricao.frame.origin.y + 15,
                               preco.frame.size.width,
                               preco.frame.size.height)];
    
    [preco setText:[[NSString stringWithFormat:@"%@ %.2f", [request moeda], i.preco] stringByReplacingOccurrencesOfString:@"." withString:[request separadorMoeda]]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
