//
//  IBVerPratos.h
//  Sophia
//
//  Created by MacCVW on 03/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBCardapio, item;

@interface IBVerPratos : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIScrollView *zoom;
    IBOutlet UILabel *titulo, *valor, *preco;
    IBOutlet UITextView *descricao;
    IBOutlet UIImageView *foto;
    IBOutlet UIButton *close;
    UIButton *ampliar;
    int estado;
}

@property (nonatomic,retain) IBCardapio *pai;
@property (nonatomic,retain) item *i;

- (IBAction)fechar:(id)sender;

@end
