//
//  request.m
//  Agraz
//
//  Created by MacCVW on 23/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "request.h"
#import "item.h"
#import "categoria.h"
#import "subcategoria.h"
#import "UIColor-Expanded.h"
#import "video.h"
#import "promocao.h"
#import <CommonCrypto/CommonDigest.h>
#import "SSZipArchive.h"

#import "AppDelegate.h"
#import "IBPublicidade.h"

NSMutableArray *pedido;
NSURLRequestCachePolicy cacheTexto = NSURLRequestReloadIgnoringCacheData;
NSURLRequestCachePolicy cacheImg = NSURLRequestReturnCacheDataElseLoad;
UINavigationController *navController;
BOOL modalAberta = NO;
NSMutableArray *botoesSelecionados;
UIButton *botaoCategoriaSelecionado;

@implementation request

+ (NSString*)idCliente
{
    return @"sushimi";
    
    NSUserDefaults *d = [NSUserDefaults standardUserDefaults];
    NSString *_idcliente = [d stringForKey:@"id_cliente"];
    if (!_idcliente || !_idcliente.length)
        return @"demo";
    return _idcliente;
}

+ (NSURL*)URLBase
{
    //return [NSURL URLWithString:@"http://stock/sushimi/"];
    return [NSURL URLWithString:@"http://177.71.249.62/sushimi/"];
}

# pragma mark - Downloads

+ (UIImage*)puxaFoto:(NSString*)strUrl eNomeImg:(NSString*)nome
{
    NSString *pathImg = [DOCSFOLDER stringByAppendingFormat:@"/%@", nome];
    
    @autoreleasepool
    {
        if (![[NSFileManager defaultManager] fileExistsAtPath:pathImg])
        {
            NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
            //NSLog(@"%@", url.absoluteString);
            NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
            NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
            [d writeToFile:pathImg atomically:YES];
        }
    }
    
    return [UIImage imageWithContentsOfFile:pathImg];
}

+ (UIImage*)puxaFotoPublicidade:(NSString*)strUrl eNomeImg:(NSString*)nome
{
    NSString *pathImg = [DOCSFOLDER stringByAppendingFormat:@"/%@", nome];
    
    @autoreleasepool
    {
        if (![[NSFileManager defaultManager] fileExistsAtPath:pathImg])
        {
            NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
            NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
            NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
            [d writeToFile:pathImg atomically:YES];
        }
    }

    return [UIImage imageWithContentsOfFile:pathImg];
}

+ (id)puxaPlist:(NSString*)strUrl
{
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[self URLBase]];
    NSError *erro;
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:cacheTexto timeoutInterval:15];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&erro];
    NSString *retorno = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];

    //retorno = [retorno stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];

    retorno = [retorno stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    retorno = [retorno stringByReplacingOccurrencesOfString:@"""" withString:@"&quot;"];
    retorno = [retorno stringByReplacingOccurrencesOfString:@"'" withString:@"&#039;"];
    
    if (erro)
        NSLog(@"%@", erro.description);
    
    return [retorno propertyList];
}

+ (void)puxaCategoriaNivel:(NSInteger)idCat
{
    NSString *strCategoria = [NSString stringWithFormat:@"request/categorias/%i/%@", idCat, [self idCliente]];
    NSArray *arrCategoria = [self puxaPlist:strCategoria];
    [arrCategoria writeToFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/%i.plist", idCat] atomically:YES];
    
    if (![arrCategoria count]) //Categoria vazia? Entao deve ter produto nela, vamu niessa.
    {
        NSString *strProdutos = [NSString stringWithFormat:@"request/produtos/%i/%@", idCat, [self idCliente]];
        NSArray *arrProdutos = [self puxaPlist:strProdutos];
        if ([arrProdutos count])
        {
            [arrProdutos writeToFile:[DOCSFOLDER stringByAppendingFormat:@"/produtos/%i.plist", idCat] atomically:YES];
            for (NSDictionary *produto in arrProdutos)
            {
                NSString *nomeFoto = [produto valueForKey:@"imagemPT"];
                NSString *strURL = [NSString stringWithFormat:@"assets/imgs/produtos/%@", nomeFoto];
                [self puxaFoto:strURL eNomeImg:nomeFoto];
            }
        }
        return;
    }
    
    for (NSDictionary *subCategoria in arrCategoria)
    {
        int idSubcat = [[subCategoria valueForKey:@"id"] intValue];
        [self puxaCategoriaNivel:idSubcat];
    }
}

+ (void)puxaMaisVendidos
{
    NSString *strProdutos = [NSString stringWithFormat:@"request/produtos/0/%@/1", [self idCliente]];
    NSArray *arrProdutos = [self puxaPlist:strProdutos];
    //NSArray *arrProdutos = [NSArray arrayWithContentsOfURL:[NSURL URLWithString:@"http://stock/sushimi/request/produtos/0/demo/1"]];
    [arrProdutos writeToFile:[DOCSFOLDER stringByAppendingFormat:@"/produtos/maisvendidos.plist"] atomically:YES];
}

+ (void)puxaSugestoes
{
    NSString *strUrl = [NSString stringWithFormat:@"request/sugestoes/%@", [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    //NSLog(@"%@", url.absoluteString);
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSArray *arraySugestoes = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary *s in arraySugestoes)
    {
        NSString *nomeFoto = [s valueForKey:@"arquivo"];
        NSString *strURL = [NSString stringWithFormat:@"assets/sugestoes/%@", nomeFoto];
        [self puxaFoto:strURL eNomeImg:nomeFoto];
    }
    
    [arraySugestoes writeToFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/sugestoes.plist"] atomically:YES];
}

+ (void)puxaVideos
{
    NSString *strUrl = [NSString stringWithFormat:@"request/videos/%@", [request idCliente]];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[request URLBase]];
    //NSLog(@"%@", url.absoluteString);
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSArray *arrVideos = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary *v in arrVideos)
    {
        NSString *nomeVideo = [v valueForKey:@"arquivo"];
        NSString *strURL = [NSString stringWithFormat:@"assets/videos/%@", nomeVideo];
        [self puxaFoto:strURL eNomeImg:nomeVideo];
    }
    
    [arrVideos writeToFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/videos.plist"] atomically:YES];
}

+ (void)puxaPublicidade:(BOOL)reload
{
    if (![self verificaConexao])
        return;
    
    @autoreleasepool
    {
        int isRetina = [[UIScreen mainScreen] scale] >= 2.0;
        isRetina++;
        NSString *strP = [NSString stringWithFormat:@"request/publicidades/%@/%i/%i", [self idCliente], 1024 * isRetina, 768 * isRetina];
        NSArray *arrFotosPublicidade = [self puxaPlist:strP];
        [arrFotosPublicidade writeToFile:[DOCSFOLDER stringByAppendingString:@"/categorias/publicidade.plist"] atomically:YES];
        
        for (NSString *urlImg in arrFotosPublicidade)
        {
            NSArray *a = [urlImg componentsSeparatedByString:@"/"];
            if ([a count] > 2)
            {
                NSString *nomeImg = [a objectAtIndex:a.count-3];
                [self puxaFotoPublicidade:urlImg eNomeImg:nomeImg];
            }
        }
    }
    
    if (reload && [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        AppDelegate *d = [[UIApplication sharedApplication] delegate];
        UINavigationController *nc = d.navigationController;
        UIViewController *p = [nc.viewControllers lastObject];

        if ([p class] == NSClassFromString(@"IBPublicidade"))
            [(IBPublicidade*)p voltar:nil];
    }
}

+ (BOOL)verificaConexao
{
    NSString *strTeste = @"request/";
    NSString *strURLTeste = [strTeste stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
    NSURL *urlTeste = [NSURL URLWithString:strURLTeste relativeToURL:[request URLBase]];
    NSMutableURLRequest *reqTeste = [[NSMutableURLRequest alloc] initWithURL:urlTeste cachePolicy:cacheTexto timeoutInterval:30];
    NSData *teste = [NSURLConnection sendSynchronousRequest:reqTeste returningResponse:nil error:nil];
    
    //NSLog(@"Puxou teste conexao");
    
    if (teste.length == 0)
        return NO;
    return YES;
}

+ (void)puxaTudo
{
    NSLog(@"%@", DOCSFOLDER);
    
    //Conteudo inicial
    if (![[NSFileManager defaultManager] fileExistsAtPath:[DOCSFOLDER stringByAppendingString:@"/1strun"]])
    {
        [SSZipArchive unzipFileAtPath:[[NSBundle mainBundle] pathForResource:@"conteudo_inicial" ofType:@"zip"] toDestination:DOCSFOLDER];
        [@"1" writeToFile:[DOCSFOLDER stringByAppendingString:@"/1strun"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    // Verifica conexão
    if (![self verificaConexao])
        return;
    
    // Puxa personalização
    @autoreleasepool
    {
        int isRetina = [[UIScreen mainScreen] scale] >= 2.0;
        isRetina++;
        NSString *strC = [NSString stringWithFormat:@"request/configuracoes/%@/%i/%i", [self idCliente], 1024 * isRetina, 768 * isRetina];
        
        NSDictionary *custom = [self puxaPlist:strC];
        [custom writeToFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"] atomically:YES];
        
        NSArray *a;
        NSString *strURL1 = [custom valueForKey:@"bg1"];
        a = [strURL1 componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [self puxaFotoPublicidade:strURL1 eNomeImg:[a objectAtIndex:a.count-3]];
        
        NSString *strURL2 = [custom valueForKey:@"bg2"];
        a = [strURL2 componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [self puxaFotoPublicidade:strURL2 eNomeImg:[a objectAtIndex:a.count-3]];
        
        NSString *strURL3 = [custom valueForKey:@"bg3"];
        a = [strURL3 componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [self puxaFotoPublicidade:strURL3 eNomeImg:[a objectAtIndex:a.count-3]];
        
        NSString *strURL4 = [NSString stringWithFormat:@"assets/imgs/configuracoes/%@", [custom valueForKey:@"logo"]];
        if (strURL4.length)
            [self puxaFoto:strURL4 eNomeImg:[custom valueForKey:@"logo"]];
    }
    
    // Puxa categorias
    @autoreleasepool
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[DOCSFOLDER stringByAppendingFormat:@"/categorias"] withIntermediateDirectories:YES attributes:nil error:nil];
        [[NSFileManager defaultManager] createDirectoryAtPath:[DOCSFOLDER stringByAppendingFormat:@"/produtos"] withIntermediateDirectories:YES attributes:nil error:nil];
        
        [self puxaCategoriaNivel:0];
    }
    
    // Puxa Sugestoes
    [self puxaSugestoes];
    
    // Puxa mais vendidos
    [self puxaMaisVendidos];
    
    // Puxa videos
    [self puxaVideos];
}

#pragma mark - App

+ (void)setNavController:(UINavigationController*)n
{
    navController = n;
}

+ (UINavigationController*)appNavController
{
    return navController;
}

+ (BOOL)fazPedidos
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]])
    {
        NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
        return [[p valueForKey:@"pedido"] boolValue];
    }
    
    return NO;
}

+ (void)setModalAberta:(BOOL)m
{
    modalAberta = m;
}

+ (BOOL)modalAberta
{
    return modalAberta;
}

# pragma mark - Sushimi

+ (BOOL)avaliarProduto:(item*)i nota:(NSUInteger)n
{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"request/avaliar/%i/%i", i.idProd, n] stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[self URLBase]];
    //NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"http://stock/sushimi/request/avaliar/%i/%i", i.idProd, n] stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:cacheTexto timeoutInterval:30];
    NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
    NSString *retorno = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    return [retorno isEqualToString:@"ok"];
}

+ (BOOL)enviarFormNome:(NSString*)n email:(NSString*)e profissao:(NSString*)p cidade:(NSString*)c aniversario:(NSString*)a newsletter:(BOOL)l
{
    NSString *post = [NSString stringWithFormat:@"nome=%@&email=%@&profissao=%@&cidade=%@&aniversario=%@&newsletter=%i", n, e, p, c, a, l];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    NSURL *url = [NSURL URLWithString:[@"request/formulario" stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding] relativeToURL:[self URLBase]];
    NSMutableURLRequest *mRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:cacheTexto timeoutInterval:30];
    [mRequest setHTTPMethod:@"POST"];
    [mRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [mRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [mRequest setHTTPBody:postData];
    
    NSData *dado = [NSURLConnection sendSynchronousRequest:mRequest returningResponse:nil error:nil];
    NSString *retorno = [[NSString alloc] initWithData:dado encoding:NSUTF8StringEncoding];
    
    return [retorno isEqualToString:@"ok"];
}

+ (video*)videoPrincipal
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[DOCSFOLDER stringByAppendingFormat:@"/categorias/videos.plist"]])
    {
        NSArray *arrDictVideos = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/videos.plist"]];
        for (NSDictionary *dictVideo in arrDictVideos)
        {
            if ([[dictVideo valueForKey:@"destaque"] boolValue])
            {
                video *v = [[video alloc] init];
                [v setPath:[DOCSFOLDER stringByAppendingFormat:@"/%@", [dictVideo valueForKey:@"arquivo"]]];
                [v setDescricao:[dictVideo valueForKey:@"descricao"]];
                return v;
            }
        }
    }
    
    return nil;
}

# pragma mark - Produtos e Categorias

+ (NSArray*)localCategoriasDoMenu:(NSUInteger)idMenu
{
    NSArray *arrayCategoriasPlist = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/%i.plist", idMenu]];
    
    NSMutableArray *arrayCategorias = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSDictionary *dictCat in arrayCategoriasPlist)
    {
        categoria *c = [[categoria alloc] init];
        [c setIdCat:[[dictCat valueForKey:@"id"] intValue]];
        [c setTituloPt:[dictCat valueForKey:@"tituloPT"]];
        [c setTituloEn:[dictCat valueForKey:@"tituloEN"]];
        [c setTituloEs:[dictCat valueForKey:@"tituloES"]];
        [arrayCategorias addObject:c];
    }
    
    return arrayCategorias;
}

+ (NSArray*)localProdutosCategoria:(NSUInteger)idCat
{
    NSArray *arrayProdutos = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/produtos/%i.plist", idCat]];
    NSMutableArray *a = [[NSMutableArray alloc] init];
    for (NSDictionary *produto in arrayProdutos)
    {
        NSString *strURL = [NSString stringWithFormat:@"assets/imgs/produtos/%@", [produto valueForKey:@"imagemPT"]];

        item *i = [[item alloc] init];
        [i setUrlFoto:[[NSURL URLWithString:strURL relativeToURL:[request URLBase]] absoluteString]];
        [i setTituloPt:[produto valueForKey:@"tituloPT"]];
        [i setTituloEn:[produto valueForKey:@"tituloEN"]];
        [i setTituloEs:[produto valueForKey:@"tituloES"]];
        [i setImagem:[produto valueForKey:@"imagemPT"]];
        [i setIdProd:[[produto valueForKey:@"id"] intValue]];
        [i setCod:[produto valueForKey:@"id"]];
        [i setPreco:[[produto valueForKey:@"preco"] floatValue]];
        [i setDescricaoPt:[produto valueForKey:@"descricaoPt"]];
        [i setDescricaoEn:[produto valueForKey:@"descricaoEn"]];
        [i setDescricaoEs:[produto valueForKey:@"descricaoEs"]];
        [a addObject:i];
    }
    return a;
}

+ (NSArray*)localSubcategoriasDaCategoria:(NSUInteger)idCat
{
    NSArray *arraySubcategoriasPlist = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/%i.plist", idCat]];
    
    NSMutableArray *subCats = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSDictionary *subCat in arraySubcategoriasPlist)
    {
        subcategoria *s = [[subcategoria alloc] init];
        [s setTituloPt:[subCat valueForKey:@"tituloPT"]];
        [s setTituloEn:[subCat valueForKey:@"tituloEN"]];
        [s setTituloEs:[subCat valueForKey:@"tituloES"]];
        [s setItens:[NSArray arrayWithArray:[request localProdutosCategoria:[[subCat valueForKey:@"id"] intValue]]]];
        [subCats addObject:s];
    }

    return subCats;
}

+ (NSArray*)localFotosPublicidade
{
    NSArray *plist = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/publicidade.plist"]];
    return plist;
}

+ (NSArray*)localPromocoes
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[DOCSFOLDER stringByAppendingFormat:@"/categorias/sugestoes.plist"]])
    {
        NSArray *arrDictSugestoes = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/sugestoes.plist"]];
        NSMutableArray *arrSugestoes = [[NSMutableArray alloc] initWithCapacity:arrDictSugestoes.count];
        for (NSDictionary *dictSugestao in arrDictSugestoes)
        {
            promocao *s = [[promocao alloc] init];
            [s setImagem:[UIImage imageWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/%@", [dictSugestao valueForKey:@"arquivo"]]]];
            NSString *strURL = [NSString stringWithFormat:@"assets/sugestoes/%@", [dictSugestao valueForKey:@"arquivo"]];
            [s setUrl:[[NSURL URLWithString:strURL relativeToURL:[request URLBase]] absoluteString]];
            [arrSugestoes addObject:s];
        }
        
        return arrSugestoes;
    }
    else
        return [NSArray new];
}

+ (NSArray*)localVideos
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[DOCSFOLDER stringByAppendingFormat:@"/categorias/videos.plist"]])
    {
        NSArray *arrDictVideos = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/categorias/videos.plist"]];
        NSMutableArray *arrVideos = [[NSMutableArray alloc] initWithCapacity:arrDictVideos.count];
        for (NSDictionary *dictVideo in arrDictVideos)
        {
            video *v = [[video alloc] init];
            [v setPath:[DOCSFOLDER stringByAppendingFormat:@"/%@", [dictVideo valueForKey:@"arquivo"]]];
            [v setDescricao:[dictVideo valueForKey:@"descricao"]];
            [arrVideos addObject:v];
        }
        
        return arrVideos;
    }
    else
        return [NSArray new];
}

+ (NSArray*)localMaisVendidos
{
    NSArray *arrayProdutos = [NSArray arrayWithContentsOfFile:[DOCSFOLDER stringByAppendingFormat:@"/produtos/maisvendidos.plist"]];
    NSMutableArray *a = [[NSMutableArray alloc] init];
    for (NSDictionary *produto in arrayProdutos)
    {
        NSString *strURL = [NSString stringWithFormat:@"assets/imgs/produtos/%@", [produto valueForKey:@"imagemPT"]];

        item *i = [[item alloc] init];
        [i setUrlFoto:[[NSURL URLWithString:strURL relativeToURL:[request URLBase]] absoluteString]];
        [i setTituloPt:[produto valueForKey:@"tituloPT"]];
        [i setTituloEn:[produto valueForKey:@"tituloEN"]];
        [i setTituloEs:[produto valueForKey:@"tituloES"]];
        [i setImagem:[produto valueForKey:@"imagemPT"]];
        [i setIdProd:[[produto valueForKey:@"id"] intValue]];
        [i setCod:[produto valueForKey:@"id"]];
        [i setPreco:[[produto valueForKey:@"preco"] floatValue]];
        [i setDescricaoPt:[produto valueForKey:@"descricaoPt"]];
        [i setDescricaoEn:[produto valueForKey:@"descricaoEn"]];
        [i setDescricaoEs:[produto valueForKey:@"descricaoEs"]];
        [a addObject:i];
    }
    return a;
}

+ (UIColor*)corFontes
{
    NSString *pathPersona = [DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathPersona])
    {
        NSDictionary *p = [[NSDictionary alloc] initWithContentsOfFile:pathPersona];
        if ([[p valueForKey:@"cor"] length] > 0)
            return [UIColor colorWithHexString:[p valueForKey:@"cor"]];
    }
    return [UIColor whiteColor];
}

+ (UIImage*)corBotao:(CorBotao)c
{
    NSString *pathPersona = [DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathPersona])
    {
        NSDictionary *p = [[NSDictionary alloc] initWithContentsOfFile:pathPersona];
        UIColor *cor;
        
        switch (c)
        {
            case CorBotaoUp1:
                if ([[p valueForKey:@"cor_up_inicial"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_up_inicial"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    cor = [UIColor colorWithHexString:cor1];
                }
                else
                    cor = [UIColor whiteColor];
                break;
                
            case CorBotaoUp2:
                if ([[p valueForKey:@"cor_up_final"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_up_final"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    cor = [UIColor colorWithHexString:cor1];
                }
                else
                    cor = [UIColor whiteColor];
                break;
                
            case CorFundoItem:
                if ([[p valueForKey:@"cor_up_final"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_up_final"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    cor = [UIColor colorWithHexString:cor1];
                }
                else
                    cor = [UIColor whiteColor];
                break;
                
            case CorBotaoDown:
                if ([[p valueForKey:@"cor_down_inicial"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_down_inicial"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    cor = [UIColor colorWithHexString:cor1];
                }
                else
                    cor = [UIColor lightGrayColor];
                break;
                
            case CorBotaoSelecionado:
                if ([[p valueForKey:@"cor_down_final"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_down_final"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    cor = [UIColor colorWithHexString:cor1];
                }
                else
                    cor = [UIColor whiteColor];
                break;
                
            case CorBotaoVoltarUp:
                cor = [UIColor colorWithHexString:@"969696"];
                break;
                
            case CorBotaoVoltarDown:
                cor = [UIColor colorWithHexString:@"6A6A6A"];
                break;
                
            default:
                cor = [UIColor whiteColor];
        }
        
        CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        if (c != CorFundoItem)
            CGContextSetAlpha(context, 0.7);
        CGContextSetFillColorWithColor(context, [cor CGColor]);
        CGContextFillRect(context, rect);
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return image;
    }
    
    return nil;
}

+ (UIImage*)fundoBotaoTipo:(CorBotao)tipo tamanho:(CGSize)tam eBordaRaio:(float)radius
{
    NSString *pathPersona = [DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathPersona])
    {
        NSDictionary *p = [[NSDictionary alloc] initWithContentsOfFile:pathPersona];
        
        CAGradientLayer *layer = [CAGradientLayer layer];
        layer.frame = CGRectMake(0, 0, tam.width, tam.height);
        [layer setStartPoint:CGPointMake(0, 0)];
        [layer setEndPoint:CGPointMake(0, 1)];
        
        switch (tipo)
        {
            case CorBotaoUp1:
                if ([[p valueForKey:@"cor_up_inicial"] length] > 0 && [[p valueForKey:@"cor_up_final"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_up_inicial"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    NSString *cor2 = [[p valueForKey:@"cor_up_final"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    
                    layer.colors = [NSArray arrayWithObjects:
                                    (id)[[UIColor colorWithHexString:cor1] CGColor],
                                    (id)[[UIColor colorWithHexString:cor2] CGColor],
                                    nil];
                }
                else
                    layer.colors = [NSArray arrayWithObjects:
                                    (id)[[UIColor darkGrayColor] CGColor],
                                    (id)[[UIColor blackColor] CGColor],
                                    nil];
                break;
                
            case CorBotaoDown:
                if ([[p valueForKey:@"cor_down_inicial"] length] > 0 && [[p valueForKey:@"cor_down_final"] length] > 0)
                {
                    NSString *cor1 = [[p valueForKey:@"cor_down_inicial"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    NSString *cor2 = [[p valueForKey:@"cor_down_final"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
                    
                    layer.colors = [NSArray arrayWithObjects:
                                    (id)[[UIColor colorWithHexString:cor1] CGColor],
                                    (id)[[UIColor colorWithHexString:cor2] CGColor],
                                    nil];
                }
                else
                    layer.colors = [NSArray arrayWithObjects:
                                    (id)[[UIColor darkGrayColor] CGColor],
                                    (id)[[UIColor blackColor] CGColor],
                                    nil];
                break;
                
            default:
                break;
        }
        
        return [self roundCorners:[self imageFromLayer:layer] withRadius:radius];
    }
    
    return [UIImage imageNamed:@"botao_inicial.png"];
}

+ (UIColor*)corBordaTipo:(CorBotao)tipo
{
    return [UIColor clearColor];
}

+ (NSString*)moeda
{
    NSString *pathPersona = [DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathPersona])
    {
        NSDictionary *p = [[NSDictionary alloc] initWithContentsOfFile:pathPersona];
        if ([[p valueForKey:@"moeda"] length] > 0)
            return [p valueForKey:@"moeda"];
    }
    return @"$";
}

+ (NSString*)separadorMoeda
{
    if ([[self moeda] rangeOfString:@"US"].location != NSNotFound)
        return @".";
    return @",";
}

# pragma mark - Outros

+ (UIImage*)resize:(UIImage*)img novoTamanho:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (NSString*)pathPara:(NSString*)file
{
    NSString *pathImg = [DOCSFOLDER stringByAppendingFormat:@"/%@", file];
    
    /*
     if (![[NSFileManager defaultManager] fileExistsAtPath:pathImg]) //puxemos se nao ecsiste
     {
     NSString *strURL = [NSString stringWithFormat:@"assets/imgs/produtos/%@", file];
     strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
     NSURL *url = [NSURL URLWithString:strURL relativeToURL:[request URLBase]];
     NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:cacheImg timeoutInterval:30];
     NSData *d = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil];
     [d writeToFile:pathImg atomically:YES];
     //NSLog(@"Puxou: %@", file);
     }
     */
    
    return pathImg;
}

+ (UIImage *)imageFromLayer:(CALayer *)layer
{
    UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

+ (void)selecionaBotaoCategoria:(UIButton*)btn
{
    if (botaoCategoriaSelecionado)
        [botaoCategoriaSelecionado setSelected:NO];
    [btn setSelected:YES];
    botaoCategoriaSelecionado = btn;
}

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

+ (UIImage *)roundCorners:(UIImage*)img withRadius:(float)radius {
	int w = img.size.width;
	int h = img.size.height;
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
	
	CGContextBeginPath(context);
	CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
	addRoundedRectToPath(context, rect, radius, radius);
	CGContextClosePath(context);
	CGContextClip(context);
	
	CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
	
	CGImageRef imageMasked = CGBitmapContextCreateImage(context);
	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);
	
	return [UIImage imageWithCGImage:imageMasked];
}

@end