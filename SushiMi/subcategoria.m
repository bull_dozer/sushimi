//
//  subcategoria.m
//  Agraz
//
//  Created by MacCVW on 02/05/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "subcategoria.h"
#import "request.h"
#import "Language.h"

@implementation subcategoria
@synthesize cod, itens, tituloPt, tituloEn, tituloEs;

- (NSString*)titulo
{
    return tituloPt;
}

@end
