//
//  IBPublicidade.h
//  Sophia
//
//  Created by MacCVW on 06/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBPublicidade : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIButton *btnVoltar;
    NSTimer *timer;
}

- (IBAction)voltar:(id)sender;

@end
