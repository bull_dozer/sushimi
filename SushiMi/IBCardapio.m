//
//  IBCardapio.m
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "IBCardapio.h"
#import "cellCategoria.h"
#import "tabelaItens.h"
#import "request.h"
#import "categoria.h"
#import "pedido.h"
#import "Language.h"
#import "item.h"
#import "video.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation IBCardapio
@synthesize tItens;

# pragma mark - Tabela

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[request localCategoriasDoMenu:currentIDCat] count] + (arrayCategorias.count != 0);
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (cellCategoria*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *identifier = @"Cell";
    NSString *identifier = [NSString stringWithFormat:@"%i-%i-%i", currentIDCat, indexPath.section, indexPath.row];
    cellCategoria *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        cell = [[cellCategoria alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setPai:self];
    }
    
    if (indexPath.row == [[request localCategoriasDoMenu:currentIDCat] count] && arrayCategorias.count)
    {
        [cell.btn setTitle:[Language get:@"VOLTAR"] forState:UIControlStateNormal];
        [cell setEhVoltar:YES];
    }
    else
    {
        categoria *c = [[request localCategoriasDoMenu:currentIDCat] objectAtIndex:indexPath.row];
        [cell.btn setTitle:c.titulo forState:UIControlStateNormal];
        [cell setIdCat:c.idCat];
        [cell setEhVoltar:NO];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)acaoCarregaMenu:(NSArray*)info
{
    NSNumber *idCat = [info objectAtIndex:0];
    NSString *nomeCat = [info objectAtIndex:1];
    
    [self.tabelaItensDelegate setTipo:TabelaItens];
    
    if ([[request localProdutosCategoria:[idCat intValue]] count])
    {
        [self.tabelaItensDelegate setItens:[request localProdutosCategoria:[idCat intValue]]];
        [self.tabelaItensDelegate setEsconderIcons:[[nomeCat uppercaseString] isEqualToString:@"BEBIDAS"]];
        [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        [arrayCategorias addObject:[NSNumber numberWithInt:currentIDCat]];
        currentIDCat = [idCat intValue];
        [tCat reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

- (void)carregaMenu:(NSInteger)idCat nome:(NSString*)nomeCat
{
    [self performSelector:@selector(acaoCarregaMenu:) withObject:[NSArray arrayWithObjects:[NSNumber numberWithInt:idCat], nomeCat, nil] afterDelay:0.2];
}

- (void)acaoVoltaMenu
{
    if (arrayCategorias.count)
    {
        currentIDCat = [[arrayCategorias lastObject] intValue];
        [arrayCategorias removeLastObject];
    }
    else
        currentIDCat = 0;
    
    [tCat reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
}

- (void)voltaMenu
{
    [self performSelector:@selector(acaoVoltaMenu) withObject:nil afterDelay:0.2];
}

# pragma mark - Menu

- (IBAction)minhaEscolha:(id)sender
{
    [request selecionaBotaoCategoria:sender];
    [sender setSelected:YES];
    
    [self.tabelaItensDelegate setTipo:TabelaEscolha];
    [self.tabelaItensDelegate setEsconderIcons:NO];
    
    [self.tabelaItensDelegate setItens:[pedido pedido]];
    [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
}

- (IBAction)promocoes:(id)sender
{
    [request selecionaBotaoCategoria:sender];
    [sender setSelected:YES];
    
    [self.tabelaItensDelegate setTipo:TabelaPromo];
    [self.tabelaItensDelegate setEsconderIcons:NO];
    
    [self.tabelaItensDelegate setItens:[request localPromocoes]];
    [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)maisVendidos:(id)sender
{
    [request selecionaBotaoCategoria:sender];
    [sender setSelected:YES];
    
    [self.tabelaItensDelegate setTipo:TabelaItens];
    [self.tabelaItensDelegate setEsconderIcons:NO];
    
    [self.tabelaItensDelegate setItens:[request localMaisVendidos]];
    [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)videos:(id)sender
{
    [request selecionaBotaoCategoria:sender];
    [sender setSelected:YES];
    
    [self.tabelaItensDelegate setTipo:TabelaVideos];
    [self.tabelaItensDelegate setEsconderIcons:NO];
    
    [self.tabelaItensDelegate setItens:[request localVideos]];
    
    [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)cadastro:(id)sender
{
    [request selecionaBotaoCategoria:sender];
    [sender setSelected:YES];
    
    [self.tabelaItensDelegate setTipo:TabelaCadastro];
    [self.tabelaItensDelegate setEsconderIcons:NO];

    [tItens reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

# pragma mark - App

- (void)fechaVideo:(UIButton*)sender
{
    [self fundoBranco];
    [sender setUserInteractionEnabled:NO];
    [moviePlayer pause];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         [moviePlayer.view setAlpha:0];
         [sender setAlpha:0];
     } completion:^(BOOL finished)
     {
         [sender removeFromSuperview];
         [moviePlayer.view removeFromSuperview];
         moviePlayer = nil;
     }];
}

- (void)abreVideo:(video*)v
{
    NSLog(@"Path: %@", v.path);
    
    if (!moviePlayer)
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:v.path]];
    else
        [moviePlayer setContentURL:[NSURL fileURLWithPath:v.path]];
    
    [moviePlayer.view setFrame:CGRectMake(0, 0, 800, 600)];
    [moviePlayer setControlStyle:MPMovieControlStyleEmbedded];
    [moviePlayer.view setCenter:self.view.center];
    [moviePlayer.view setAlpha:0];
    [moviePlayer.view.layer setBorderWidth:4];
    [moviePlayer.view.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [moviePlayer prepareToPlay];
    [self.view addSubview:moviePlayer.view];
    
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    [close setImage:[UIImage imageNamed:@"box_close.png"] forState:UIControlStateNormal];
    [close sizeToFit];
    [close setCenter:CGPointMake(moviePlayer.view.frame.origin.x + 5, moviePlayer.view.frame.origin.y + 5)];
    [close addTarget:self action:@selector(fechaVideo:) forControlEvents:UIControlEventTouchUpInside];
    [close setAlpha:0];
    [close setTag:555];
    [self.view addSubview:close];
    
    [self fundoBranco];
    [UIView animateWithDuration:0.5 animations:^
     {
         [moviePlayer.view setAlpha:1];
         [close setAlpha:1];
     }];
}

- (void)fechaRating:(UIButton*)sender
{
    [self fundoBranco];
    [sender setUserInteractionEnabled:NO];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         [viewRating setAlpha:0];
         [sender setAlpha:0];
     } completion:^(BOOL finished)
     {
         [sender removeFromSuperview];
         [viewRating removeFromSuperview];
         currentItem = nil;
     }];
}

- (IBAction)escolheEstrela:(id)sender
{
    int tagEstrega = [sender tag];
    UIButton *close = (UIButton*)[self.view viewWithTag:555];
    
    for (int i = 1; i <= 5; i++)
    {
        UIButton *star = (UIButton*)[viewRating viewWithTag:i];
        if (i <= tagEstrega)
            [star setAlpha:1];
        else
            [star setAlpha:0.5];
    }
    
    if([request avaliarProduto:currentItem nota:tagEstrega])
    {
        [close setUserInteractionEnabled:NO];
        [viewRating setUserInteractionEnabled:NO];
        [self performSelector:@selector(fechaRating:) withObject:close afterDelay:0.5];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Erro" message:@"Ocorreu um erro. Por favor, tente novamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (void)abreRatingItem:(item*)i
{
    currentItem = i;
    
    [self fundoBranco];
    [viewRating setUserInteractionEnabled:YES];
    [viewRating setCenter:self.view.center];
    [viewRating setAlpha:0];
    [viewRating.layer setBorderWidth:4];
    [viewRating.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [self.view addSubview:viewRating];
    [tituloRating setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
    [tituloRating setText:[NSString stringWithFormat:@"Dê uma nota para %@", i.titulo]];
    
    for (int i = 1; i <= 5; i++)
    {
        UIButton *star = (UIButton*)[viewRating viewWithTag:i];
        [star setAlpha:0.5];
    }
    
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    [close setImage:[UIImage imageNamed:@"box_close.png"] forState:UIControlStateNormal];
    [close sizeToFit];
    [close setCenter:CGPointMake(viewRating.frame.origin.x + 5, viewRating.frame.origin.y + 5)];
    [close addTarget:self action:@selector(fechaRating:) forControlEvents:UIControlEventTouchUpInside];
    [close setAlpha:0];
    [close setTag:555];
    [self.view addSubview:close];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         [viewRating setAlpha:1];
         [close setAlpha:1];
     }];
}

- (void)fechaZoom:(UIButton*)sender
{
    [self fundoBranco];
    [sender setUserInteractionEnabled:NO];

    [UIView animateWithDuration:0.5 animations:^
     {
         [sender setAlpha:0];
         [imgZoom setAlpha:0];
     } completion:^(BOOL finished)
     {
         [sender removeFromSuperview];
         [imgZoom removeFromSuperview];
         imgZoom = nil;
     }];
}

- (void)abreZoom:(UIImage*)foto
{
    if (!foto)
        return;
    
    [self fundoBranco];
    imgZoom = [[UIImageView alloc] initWithImage:foto];
    [imgZoom sizeToFit];
    [imgZoom setCenter:self.view.center];
    [imgZoom.layer setBorderWidth:4];
    [imgZoom.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [imgZoom setAlpha:0];
    [self.view addSubview:imgZoom];
    
    UIButton *close = [UIButton buttonWithType:UIButtonTypeCustom];
    [close setImage:[UIImage imageNamed:@"box_close.png"] forState:UIControlStateNormal];
    [close sizeToFit];
    [close setCenter:CGPointMake(imgZoom.frame.origin.x + 5, imgZoom.frame.origin.y + 5)];
    [close addTarget:self action:@selector(fechaZoom:) forControlEvents:UIControlEventTouchUpInside];
    [close setAlpha:0];
    [close setTag:555];
    [self.view addSubview:close];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         [imgZoom setAlpha:1];
         [close setAlpha:1];
     }];
}

- (void)avisaPedido
{
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"PEDIDO"] message:[Language get:@"PEDIDOENVIADO"] delegate:nil cancelButtonTitle:[Language get:@"OK"] otherButtonTitles:nil];
    [a show];
    [self voltar:nil];
}

- (IBAction)voltar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)encerraPedido:(id)sender
{
    [FBSession.activeSession closeAndClearTokenInformation];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    
    [pedido zeraPedido];
    [self voltar:nil];
}

- (void)fundoBranco
{
    float alphaDestino = 0.0;
    if (branco.alpha == 0.0)
        alphaDestino = 0.4;
    [UIView animateWithDuration:0.5 animations:^
    {
        [branco setAlpha:alphaDestino];
    }];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)ajustaPersonalizacao
{
    NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
    
    if ([[p valueForKey:@"bg2"] length] > 0)
    {
        NSArray *a = [[p valueForKey:@"bg2"] componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [background setImage:[UIImage imageWithContentsOfFile:[request pathPara:[a objectAtIndex:a.count-3]]]];
    }
    
    int isRetina = [[UIScreen mainScreen] scale] >= 2.0;
    isRetina++;
    
    if ([[p valueForKey:@"logo"] length] > 0)
    {
        UIImage *logoOriginal = [UIImage imageWithContentsOfFile:[request pathPara:[p valueForKey:@"logo"]]];
        
        float novaAltura = (logoOriginal.size.height * 612) / logoOriginal.size.width; //regra de 3 - aprendi na escola
        
        UIImage *scaledLogo = [self imageWithImage:logoOriginal scaledToSize:CGSizeMake(612 * isRetina, novaAltura * isRetina)];
        [logo setImage:scaledLogo];
    }
    
    static UIImage *fundoUp, *fundoDown, *fundoSelected;
    if (!fundoUp)
        fundoUp = [request corBotao:CorBotaoUp1];
    if (!fundoDown)
        fundoDown = [request corBotao:CorBotaoDown];
    if (!fundoSelected)
        fundoSelected = [request corBotao:CorBotaoSelecionado];
    
    for (UIButton *btn in [NSArray arrayWithObjects:self.btnVideos, self.btnCadastro, self.btnEscolha, self.btnSugestoes, self.btnMaisVendidos, nil])
    {
        [btn setBackgroundImage:fundoUp forState:UIControlStateNormal];
        [btn setBackgroundImage:fundoDown forState:UIControlStateHighlighted];
        [btn setBackgroundImage:fundoSelected forState:UIControlStateSelected];
        [btn.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
        [btn setTitleColor:[request corFontes] forState:UIControlStateNormal];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(7, 0, 0, 0)];
        [btn.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [btn.layer setBorderWidth:1];
        [btn.layer setBorderColor:[[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1] CGColor]];
    }
}

- (void)abreCombos
{
    for (int j = 0; j < [tCat numberOfRowsInSection:0]; j++)
    {
        NSUInteger ints[2] = {0,j};
        NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
        categoria *c = [[request localCategoriasDoMenu:currentIDCat] objectAtIndex:indexPath.row];
        
        if ([[c.titulo uppercaseString] isEqualToString:@"COMBOS"])
        {
            cellCategoria *cell = (cellCategoria*)[tCat cellForRowAtIndexPath:indexPath];
            [cell acaoBtn];
            break;
        }
    }
}

- (void)viewDidLoad
{
    currentIDCat = 0;
    [super viewDidLoad];
    
    [self ajustaPersonalizacao];
    
    self.tabelaItensDelegate = [[tabelaItens alloc] init];
    [self.tabelaItensDelegate setPai:self];
    [tItens setDelegate:self.tabelaItensDelegate];
    [tItens setDataSource:self.tabelaItensDelegate];
    
    [self.btnVoltar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:20]];
    [self.btnVoltar setTitleEdgeInsets:UIEdgeInsetsMake(6, 25, 0, 0)];
    [self.btnVoltar setTitle:[Language get:@"VOLTARM"] forState:UIControlStateNormal];
    
    [self.btnPedido.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:20]];
    [self.btnPedido setTitleEdgeInsets:UIEdgeInsetsMake(6, -25, 0, 0)];
    [self.btnPedido setTitle:[Language get:@"VERPEDIDO"] forState:UIControlStateNormal];
    
    arrayCategorias = [[NSMutableArray alloc] initWithCapacity:0];
    
    [self abreCombos];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
