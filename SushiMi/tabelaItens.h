//
//  tabelaItens.h
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    TabelaItens = 1,
    TabelaPromo,
    TabelaVideos,
    TabelaCadastro,
    TabelaEscolha
} TabelaTipo;

@class IBCardapio;

@interface tabelaItens : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property TabelaTipo tipo;
@property (nonatomic,strong) NSArray *itens;
@property (nonatomic,strong) IBCardapio *pai;
@property BOOL esconderIcons;

@end
