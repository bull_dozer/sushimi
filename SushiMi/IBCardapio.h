//
//  IBCardapio.h
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class tabelaItens, item, video, MPMoviePlayerController;

@interface IBCardapio : UIViewController <UITableViewDelegate, UITableViewDataSource, FBLoginViewDelegate>
{
    IBOutlet UITableView *tItens, *tCat;
    IBOutlet UIImageView *branco, *logo, *background;
    IBOutlet UIView *viewRating;
    IBOutlet UILabel *tituloRating;
    
    NSInteger currentIDCat;
    NSMutableArray *arrayCategorias;
    
    MPMoviePlayerController *moviePlayer;
    UIImageView *imgZoom;
    
    item *currentItem;
}

@property (nonatomic,strong) IBOutlet UIButton *btnVoltar, *btnPedido, *btnVideos, *btnCadastro, *btnEscolha, *btnSugestoes, *btnMaisVendidos;
@property (nonatomic,strong) tabelaItens *tabelaItensDelegate;
@property (nonatomic,strong) IBOutlet UITableView *tItens;

- (IBAction)voltar:(id)sender;
- (void)carregaMenu:(NSInteger)idCat nome:(NSString*)nomeCat;
- (void)voltaMenu;
- (void)avisaPedido;
- (void)abreZoom:(UIImage*)foto;
- (void)fundoBranco;
- (IBAction)encerraPedido:(id)sender;

- (void)abreVideo:(video*)v;
- (void)abreRatingItem:(item*)i;
- (IBAction)escolheEstrela:(id)sender;

- (IBAction)minhaEscolha:(id)sender;
- (IBAction)promocoes:(id)sender;
- (IBAction)maisVendidos:(id)sender;
- (IBAction)videos:(id)sender;
- (IBAction)cadastro:(id)sender;

@end
