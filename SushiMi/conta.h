//
//  conta.h
//  Sophia
//
//  Created by MacCVW on 17/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DOCSFOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface conta : NSObject

+ (void)iniciaConta;
+ (void)inserePedido:(NSArray*)p;
+ (NSArray*)conta;
+ (float)totalConta;
+ (void)zeraConta;

+ (NSString*)setMesa:(NSString*)m verifica:(BOOL)v;
+ (NSString*)mesa;
+ (NSString*)transacao;

+ (NSArray*)puxaMinhaConta;
+ (BOOL)carregaContaDisco;
+ (void)salvaContaDisco;
+ (void)removeContaDisco;

+ (NSString*)solicitaFechamento;
+ (BOOL)statusMesa;

@end
