//
//  categoria.m
//  Agraz
//
//  Created by MacCVW on 27/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "categoria.h"
#import "request.h"
#import "Language.h"

@implementation categoria
@synthesize idCat, tituloPt, tituloEn, tituloEs;

- (NSString*)titulo
{
    return tituloPt;
}

@end
