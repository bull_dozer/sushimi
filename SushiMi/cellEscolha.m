//
//  cellEscolha.m
//  SushiMi
//
//  Created by MacCVW on 11/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import "cellEscolha.h"
#import "item.h"
#import "IBVerPratos.h"
#import "IBCardapio.h"
#import "request.h"
#import "pedido.h"
#import "Language.h"
#import "DiegoButton.h"

@implementation cellEscolha

- (void)abreZoom:(DiegoButton*)sender
{
    UIImage *fotoZoom = ([sender tag] == 0) ? self.i1.foto : self.i2.foto;
    [self.pai abreZoom:fotoZoom];
}

- (void)removerItem:(DiegoButton*)sender
{
    aux = ([sender tag] == 0) ? self.i1 : self.i2;
    
    if ([pedido itemEstaNoPedido:aux])
        [pedido removeItem:aux];
    
    [self.pai minhaEscolha:self.pai.btnEscolha];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        //Item 1
        self.fundo1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 169, 284, 90)];
        [self.fundo1 setImage:[request corBotao:CorFundoItem]];
        [self.contentView addSubview:self.fundo1];
        
        self.foto1 = [[DiegoButton alloc] initWithFrame:CGRectMake(10, 14, 275, 172)];
        [self.foto1.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.foto1 setTag:0];
        [self.foto1 addTarget:self action:@selector(abreZoom:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.foto1];
        
        self.titulo1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 194, 274, 25)];
        [self.titulo1 setBackgroundColor:[UIColor clearColor]];
        [self.titulo1 setTextColor:[UIColor whiteColor]];
        [self.titulo1 setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self.titulo1 setAdjustsFontSizeToFitWidth:YES];
        [self.contentView addSubview:self.titulo1];
        
        self.descricao1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 217, 274, 42)];
        [self.descricao1 setBackgroundColor:[UIColor clearColor]];
        [self.descricao1 setTextColor:[UIColor whiteColor]];
        [self.descricao1 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao1 setNumberOfLines:2];
        [self.descricao1 setLineBreakMode:NSLineBreakByWordWrapping];
        [self.contentView addSubview:self.descricao1];
        
        self.fechar1 = [[DiegoButton alloc] initWithFrame:CGRectMake(272, 1, 25, 25)];
        [self.fechar1 setImage:[UIImage imageNamed:@"remover.png"] forState:UIControlStateNormal];
        [self.fechar1 setTag:0];
        [self.fechar1 addTarget:self action:@selector(removerItem:) forControlEvents:UIControlEventTouchUpInside];
        //[self.fechar1.layer setBorderWidth:2];
        //[self.fechar1.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self addSubview:self.fechar1];
        
        //Item 2
        self.fundo2 = [[UIImageView alloc] initWithFrame:CGRectMake(315, 169, 284, 90)];
        [self.fundo2 setImage:[request corBotao:CorFundoItem]];
        [self.contentView addSubview:self.fundo2];
        
        self.foto2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 10, 14, 275, 172)];
        [self.foto2.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.foto2 setTag:1];
        [self.foto2 addTarget:self action:@selector(abreZoom:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.foto2];
        
        self.titulo2 = [[UILabel alloc] initWithFrame:CGRectMake(315 + 10, 194, 274, 25)];
        [self.titulo2 setBackgroundColor:[UIColor clearColor]];
        [self.titulo2 setTextColor:[UIColor whiteColor]];
        [self.titulo2 setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self.titulo2 setAdjustsFontSizeToFitWidth:YES];
        [self.contentView addSubview:self.titulo2];
        
        self.descricao2 = [[UILabel alloc] initWithFrame:CGRectMake(315 + 10, 217, 274, 42)];
        [self.descricao2 setBackgroundColor:[UIColor clearColor]];
        [self.descricao2 setTextColor:[UIColor whiteColor]];
        [self.descricao2 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao2 setNumberOfLines:2];
        [self.descricao2 setLineBreakMode:NSLineBreakByWordWrapping];
        [self.contentView addSubview:self.descricao2];
        
        self.fechar2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 272, 1, 25, 25)];
        [self.fechar2 setImage:[UIImage imageNamed:@"remover.png"] forState:UIControlStateNormal];
        [self.fechar2 setTag:0];
        [self.fechar2 addTarget:self action:@selector(removerItem:) forControlEvents:UIControlEventTouchUpInside];
        //[self.fechar2.layer setBorderWidth:2];
        //[self.fechar2.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self addSubview:self.fechar2];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.fundo2 setHidden:!self.i2];
    [self.foto2 setHidden:!self.i2];
    [self.titulo2 setHidden:!self.i2];
    [self.descricao2 setHidden:!self.i2];
    [self.fechar2 setHidden:!self.i2];
    
    [self.foto1 setImage:self.i1.foto forState:UIControlStateNormal];
    [self.foto2 setImage:self.i2.foto forState:UIControlStateNormal];
    
    //[self.titulo1 setText:[self.i1 titulo]];
    //[self.titulo2 setText:[self.i2 titulo]];
    
    [self.titulo1 setText:[[NSString stringWithFormat:@"%@ _ %.2f", [self.i1 titulo], [self.i1 preco]] stringByReplacingOccurrencesOfString:@"." withString:@","]];
    [self.titulo2 setText:[[NSString stringWithFormat:@"%@ _ %.2f", [self.i2 titulo], [self.i2 preco]] stringByReplacingOccurrencesOfString:@"." withString:@","]];
    
    [self.descricao1 setText:[self.i1 descricao]];
    [self.descricao2 setText:[self.i2 descricao]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end