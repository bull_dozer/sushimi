//
//  pedido.m
//  Agraz
//
//  Created by MacCVW on 16/05/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "pedido.h"
#import "item.h"
#import "conta.h"
#import "request.h"
#import "Language.h"

static NSMutableArray *listaPedidos;

@implementation pedido

+ (void)inicializa
{
    if (!listaPedidos)
        listaPedidos = [[NSMutableArray alloc] initWithCapacity:0];
}

+ (void)insereItem:(item*)i
{
    [self inicializa];
    if (![listaPedidos containsObject:i])
        [listaPedidos addObject:i];
}

+ (void)removeItem:(item*)i
{
    [self inicializa];
    
    for (item *it in listaPedidos)
        if (it.idProd == i.idProd)
        {
            [listaPedidos removeObject:it];
            return;
        }
}

+ (BOOL)itemEstaNoPedido:(item*)i
{
    [self inicializa];
    
    for (item *it in listaPedidos)
        if (it.idProd == i.idProd)
            return YES;
    return NO;
}

+ (void)zeraPedido
{
    [self inicializa];
    [listaPedidos removeAllObjects];
}

+ (NSArray*)pedido
{
    [self inicializa];
    return [NSArray arrayWithArray:listaPedidos];
}

@end
