//
//  IBInicial.m
//  Sophia
//
//  Created by MacCVW on 25/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "IBInicial.h"
#import "IBCardapio.h"
#import "IBPublicidade.h"
#import "request.h"
#import "categoria.h"
#import "IBMinhaConta.h"
#import "conta.h"
#import "pedido.h"
#import "Language.h"
#import "Base64Coder.h"
#import "video.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

#import <FacebookSDK/FBSessionTokenCachingStrategy.h>

@implementation IBInicial

- (IBAction)teclaNum:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    mesa.text = [mesa.text stringByAppendingString:[btn titleForState:UIControlStateNormal]];
}

- (IBAction)teclaApaga:(id)sender
{
    if (mesa.text.length)
        mesa.text = [mesa.text substringToIndex:mesa.text.length-1];
}

- (IBAction)teclaAvanca:(id)sender
{
    if ([request fazPedidos] && (!mesa.text.length || ![mesa.text intValue]))
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"]
                                                    message:[Language get:@"ESCOLHA_MESA"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    NSString *retorno = [conta setMesa:mesa.text verifica:([sender class] == NSClassFromString(@"UIButton"))];
    
    if ([retorno isEqualToString:@"erroa"])
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"]
                                                    message:[Language get:@"MESAABERTA"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    else if ([retorno isEqualToString:@"erroi"])
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"]
                                                    message:[Language get:@"MESAINEXISTENTE"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    else if ([retorno isEqualToString:@"erroc"])
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"]
                                                    message:[Language get:@"SEMCONEXAO"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    else if ([retorno isEqualToString:@"ok"])
    {
        [conta salvaContaDisco];
        
        /*
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
         {
             [fundoCalc setAlpha:0];
             [mesa setAlpha:0];
             [tituloMesa setAlpha:0];
             for (UIButton *btn in self.view.subviews)
                 if (btn.tag == 5)
                     [btn setAlpha:0];
             
         } completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.8 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
              {
                  CGRect frame;
                  frame = barra.frame;
                  frame.origin.x = 620;
                  [barra setFrame:frame];
                  [barra setAlpha:1];
                  
                  frame = logo.frame;
                  frame.origin.x = 642;
                  [logo setFrame:frame];
                  [logo setAlpha:1];
                  
                  frame = viewBandeiras.frame;
                  frame.origin.x = 620;
                  [viewBandeiras setFrame:frame];
                  [viewBandeiras setAlpha:1];
                  
              } completion:^(BOOL finished)
              {
                  [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
                   {
                       [btnCardapio setAlpha:1];
                       
                   } completion:^(BOOL finished)
                   {
                       [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
                        {
                            [btnPublicidade setAlpha:1];
                            
                        } completion:^(BOOL finished)
                        {
                            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
                             {
                                 [btnConta setAlpha:[request fazPedidos]];
                                 
                             } completion:nil];
                        }];
                   }];
              }];
         }];
         */
    }
}

- (IBAction)abreCardapio:(id)sender
{
    if ([loading isAnimating])
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"AGUARDE_SINC"]
                                                    message:nil
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    IBCardapio *c = [[IBCardapio alloc] init];
    [self.navigationController pushViewController:c animated:YES];
}

- (IBAction)abrePublicidade:(id)sender
{
    if ([loading isAnimating])
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"AGUARDE_SINC"]
                                                    message:nil
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
        [a show];
        return;
    }
    
    IBPublicidade *p = [[IBPublicidade alloc] init];
    [self.navigationController pushViewController:p animated:YES];
}

- (void)terminaSync
{
    [loading stopAnimating];
    [self ajustaPersonalizacao];
}

- (void)puxaTudo
{
    [request puxaTudo];
    [self performSelectorInBackground:@selector(terminaSync) withObject:nil];
}

- (void)iniciaSync
{
    [loading startAnimating];
    
    [[NSOperationQueue currentQueue] addOperationWithBlock:^
    {
        [self performSelectorInBackground:@selector(puxaTudo) withObject:nil];
    }];
}

- (void)ajustaPersonalizacao
{
    video *v = [request videoPrincipal];
    if (v && moviePlayer == nil)
    {
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:v.path]];
        [moviePlayer setShouldAutoplay:NO];
        [moviePlayer prepareToPlay];
        [moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        [moviePlayer.view setUserInteractionEnabled:NO];
        [self.view addSubview:moviePlayer.view];
        
        /*
         MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:v.path]];
         [mp setShouldAutoplay:NO];
         [mp prepareToPlay];
         [mp.view setFrame:CGRectMake(0, 0, 1024, 768)];
         mp.controlStyle = MPMovieControlStyleNone;
         [mp.view setUserInteractionEnabled:NO];
         [self.view addSubview:mp.view];
         */
        
        [self.view bringSubviewToFront:btnCardapio];
        [self.view bringSubviewToFront:loading];
        [self.view bringSubviewToFront:logo];
    }
    
    NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
    
    static UIImage *fundoUp, *fundoDown;
    if (!fundoUp)
        fundoUp = [request corBotao:CorBotaoUp1];
    if (!fundoDown)
        fundoDown = [request corBotao:CorBotaoDown];
    
    [btnCardapio setBackgroundImage:fundoUp forState:UIControlStateNormal];
    [btnCardapio setBackgroundImage:fundoDown forState:UIControlStateHighlighted];
    [btnCardapio.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:28]];
    [btnCardapio setTitleColor:[request corFontes] forState:UIControlStateNormal];
    [btnCardapio setTitleEdgeInsets:UIEdgeInsetsMake(11, 0, 0, 0)];
    [btnCardapio.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [btnCardapio.layer setBorderWidth:1];
    [btnCardapio.layer setBorderColor:[[UIColor colorWithRed:1 green:1 blue:1 alpha:0.1] CGColor]];
    
    if ([[p valueForKey:@"bg1"] length] > 0)
    {
        NSArray *a = [[p valueForKey:@"bg1"] componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [background setImage:[UIImage imageWithContentsOfFile:[request pathPara:[a objectAtIndex:a.count-3]]]];
    }
    
    if ([[p valueForKey:@"logo"] length] > 0)
        [logo setImage:[UIImage imageWithContentsOfFile:[request pathPara:[p valueForKey:@"logo"]]]];
    
    [tituloMesa setTextColor:[request corFontes]];
    
    [UIView animateWithDuration:0.3 animations:^
    {
        [btnCardapio.titleLabel setAlpha:0];
        //[btnPublicidade.titleLabel setAlpha:0];
        //[btnConta.titleLabel setAlpha:0];
        
    } completion:^(BOOL finished)
    {
        //[btnCardapio setTitle:[Language get:@"CARDAPIO"] forState:UIControlStateNormal];
        //[btnPublicidade setTitle:[Language get:@"PUBLICIDADE"] forState:UIControlStateNormal];
        //[btnConta setTitle:[Language get:@"MINHA_CONTA"] forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.3 animations:^
         {
             [btnCardapio.titleLabel setAlpha:1];
             //[btnPublicidade.titleLabel setAlpha:1];
             //[btnConta.titleLabel setAlpha:1];
             
         } completion:nil];
    }];
    
    [tituloMesa setText:[NSString stringWithFormat:@"%@ %@", [Language get:@"MESA"], [Language get:@"NUM"]]];
    
    if (![request fazPedidos])
        [self teclaAvanca:nil];
}

- (IBAction)setIdioma:(id)sender
{
    switch ([sender tag])
    {
        case 0:
            [Language setLanguage:@"en"];
            break;
            
        case 1:
            [Language setLanguage:@"es"];
            break;
            
        case 2:
            [Language setLanguage:@"pt"];
            break;
            
        default:
            [Language setLanguage:@"en"];
            break;
    }
    
    [self ajustaPersonalizacao];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!buttonIndex)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://cvwsolucoes.com.br/contatos.html"]];
    }
}

- (void)reloadPublicidade
{
    [request puxaPublicidade:YES];
}

-(void)moviePlayerPlaybackStateChanged:(NSNotification *)notification
{
    MPMoviePlayerController *mp = notification.object;
    if (mp == moviePlayer && mp.playbackState == MPMoviePlaybackStatePaused)
        [mp play];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Language initialize];
    
    [self ajustaPersonalizacao];

    [self performSelectorOnMainThread:@selector(iniciaSync) withObject:nil waitUntilDone:YES];
    
    video *v = [request videoPrincipal];
    if (v)
    {
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:v.path]];
        [moviePlayer setShouldAutoplay:NO];
        [moviePlayer prepareToPlay];
        [moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
        moviePlayer.controlStyle = MPMovieControlStyleNone;
        [moviePlayer.view setUserInteractionEnabled:NO];
        [self.view addSubview:moviePlayer.view];
        
        /*
         MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:v.path]];
         [mp setShouldAutoplay:NO];
         [mp prepareToPlay];
         [mp.view setFrame:CGRectMake(0, 0, 1024, 768)];
         mp.controlStyle = MPMovieControlStyleNone;
         [mp.view setUserInteractionEnabled:NO];
         [self.view addSubview:mp.view];
         */
        
        [self.view bringSubviewToFront:btnCardapio];
        [self.view bringSubviewToFront:loading];
        [self.view bringSubviewToFront:logo];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackStateChanged:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
}

- (IBAction)showEA:(id)sender
{
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Sophia" message:[Base64Coder decodeString:@"RGVzZW52b2x2aWRvIHBvciBEaWVnbyBUcmV2aXNhbiBMYXJhIChpT1MpLCBHZXJtZXJzb24gUmliYXMgKEFuZHJvaWQpLCBOZXRvIFJhbWFsaG8gKFBIUCkgZSBHdWlsaGVybWUgVml0b3IgKFBIUCku" withEncodingOrZero:NSUTF8StringEncoding] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [a show];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (moviePlayer)
        [moviePlayer stop];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (moviePlayer)
        [moviePlayer play];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
