//
//  IBMinhaConta.m
//  Sophia
//
//  Created by MacCVW on 12/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "IBMinhaConta.h"
#import "request.h"
//#import "cellMinhaConta.h"
#import "conta.h"
#import "pedido.h"
#import "IBInicial.h"
#import "Language.h"
#import "AppDelegate.h"
#import "TimerApp.h"
#import "item.h"

@implementation IBMinhaConta
@synthesize pai;

# pragma mark - Tabela

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[conta conta] count];
}
*/

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22;
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 1;
    
    return arrayPedidoProdutos.count;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSArray *pedido = [[conta conta] objectAtIndex:indexPath.section];
    //return 22 + ([pedido count] * 22);
    
    return 22;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cellMinhaConta *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        //cell = [[cellMinhaConta alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        
        UILabel *qtd = [[UILabel alloc] initWithFrame:CGRectMake(700, 0, 40, 22)];
        [qtd setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [qtd setShadowOffset:CGSizeMake(0, 1)];
        [qtd setShadowColor:[UIColor blackColor]];
        [qtd setTextColor:[UIColor whiteColor]];
        [qtd setBackgroundColor:[UIColor clearColor]];
        [qtd setTextAlignment:UITextAlignmentCenter];
        [cell.contentView addSubview:qtd];
        
        UILabel *preco = [[UILabel alloc] initWithFrame:CGRectMake(780, 0, 100, 22)];
        [preco setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
        [preco setShadowOffset:CGSizeMake(0, 1)];
        [preco setShadowColor:[UIColor blackColor]];
        [preco setTextColor:[UIColor whiteColor]];
        [preco setBackgroundColor:[UIColor clearColor]];
        [preco setTextAlignment:UITextAlignmentRight];
        [cell.contentView addSubview:preco];
    }
    
    /*
     NSArray *pedido = [[conta conta] objectAtIndex:indexPath.section];
     [cell setNumPedido:indexPath.section+1];
     [cell setP:pedido];
     return cell;
    */
    
    NSDictionary *dictItem = [arrayPedidoProdutos objectAtIndex:indexPath.row];
    item *it = [dictItem valueForKey:@"item"];
    
    [cell.textLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setShadowColor:[UIColor blackColor]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setText:[NSString stringWithFormat:@"%@", it.titulo]];
    
    UILabel *qtd = [cell.contentView.subviews objectAtIndex:0];
    [qtd setText:[NSString stringWithFormat:@"%@", [dictItem valueForKey:@"quantidade"]]];
    
    UILabel *preco = [cell.contentView.subviews objectAtIndex:1];
    float totalProduto = [[dictItem valueForKey:@"quantidade"] intValue] * it.preco;
    [preco setText:[[NSString stringWithFormat:@"%@ %.2f", [request moeda], totalProduto] stringByReplacingOccurrencesOfString:@"." withString:[request separadorMoeda]]];
    
    return cell;
}

# pragma mark - App

- (void)fechouConta
{
    if ([conta statusMesa])
    {
        [request setModalAberta:NO];
        TimerApp *a = (TimerApp*)[UIApplication sharedApplication];
        [a resetIdleTimer];
        
        [conta zeraConta];
        [conta removeContaDisco];
        
        [alertViewWaiting dismissWithClickedButtonIndex:0 animated:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        if (timerWaiting)
        {
            [timerWaiting invalidate];
            timerWaiting = nil;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex)
    {
        NSString *retorno = [conta solicitaFechamento];
        
        if ([retorno isEqualToString:@"ok"])
        {
            [request setModalAberta:YES];
            
            alertViewWaiting = [[UIAlertView alloc] initWithTitle:[Language get:@"CONTA"]
                                                        message:[Language get:@"FECHOUCONTA"]
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil];
            
            UIActivityIndicatorView *aiv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [aiv setFrame:CGRectMake(127, 100, 30, 30)];
            [alertViewWaiting addSubview:aiv];
            [aiv startAnimating];
            
            [alertViewWaiting show];
            
            timerWaiting = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(fechouConta) userInfo:nil repeats:YES];
        }
        
        else if ([retorno rangeOfString:@"erro"].location != NSNotFound)
        {
            UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"]
                                                        message:[Language get:@"ABORTAR2"]
                                                       delegate:nil
                                              cancelButtonTitle:[Language get:@"OK"]
                                              otherButtonTitles:nil];
            [a show];
            
            [self voltar:nil];
            [pai performSelector:@selector(resetMesa:) withObject:nil afterDelay:0.3];
        }
        
        else
        {
            UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"ERRO"] message:[Language get:@"SEMCONEXAO"] delegate:nil cancelButtonTitle:[Language get:@"OK"] otherButtonTitles:nil];
            [a show];
        }
    }
}

- (IBAction)fecharConta:(id)sender
{
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:[Language get:@"CONTA"]
                                                message:[Language get:@"DESEJAFECHAR"]
                                               delegate:self
                                      cancelButtonTitle:[Language get:@"NAO"]
                                      otherButtonTitles:[Language get:@"SIM"], nil];
    [a show];
}

- (IBAction)voltar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)ajustaPersonalizacao
{
    NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
    
    if ([[p valueForKey:@"bg3"] length] > 0)
    {
        NSArray *a = [[p valueForKey:@"bg3"] componentsSeparatedByString:@"/"];
        if (a.count > 2)
            [background setImage:[UIImage imageWithContentsOfFile:[request pathPara:[a objectAtIndex:a.count-3]]]];
    }
    
    [btnVoltar setBackgroundImage:[request fundoBotaoTipo:CorBotaoUp1
                                                  tamanho:CGSizeMake(btnVoltar.frame.size.width, btnVoltar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateNormal];
    [btnVoltar setBackgroundImage:[request fundoBotaoTipo:CorBotaoDown
                                                  tamanho:CGSizeMake(btnVoltar.frame.size.width, btnVoltar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateHighlighted];
    [btnVoltar.layer setBorderWidth:1];
    [btnVoltar.layer setCornerRadius:8];
    btnVoltar.layer.shouldRasterize = YES;
    btnVoltar.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    btnVoltar.imageView.layer.shouldRasterize = YES;
    btnVoltar.imageView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    UIImageView *seta1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seta_esquerda.png"]];
    [btnVoltar addSubview:seta1];
    [seta1 setFrame:CGRectMake(8, 5.5, 25, 25)];
    
    
    [btnFinalizar setBackgroundImage:[request fundoBotaoTipo:CorBotaoUp1
                                                  tamanho:CGSizeMake(btnFinalizar.frame.size.width, btnFinalizar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateNormal];
    [btnFinalizar setBackgroundImage:[request fundoBotaoTipo:CorBotaoDown
                                                  tamanho:CGSizeMake(btnFinalizar.frame.size.width, btnFinalizar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateHighlighted];
    [btnFinalizar.layer setBorderWidth:1];
    [btnFinalizar.layer setCornerRadius:8];
    btnFinalizar.layer.shouldRasterize = YES;
    btnFinalizar.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    btnFinalizar.imageView.layer.shouldRasterize = YES;
    btnFinalizar.imageView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    UIImageView *seta2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seta_direita.png"]];
    [btnFinalizar addSubview:seta2];
    [seta2 setFrame:CGRectMake(130, 5.5, 25, 25)];
    
    //[fundo setBackgroundColor:[request corFontes]];
    [fundo setBackgroundColor:[request corBordaTipo:CorBotaoUp1]];
    [fundo setAlpha:0.5];
    [fundo.layer setCornerRadius:13];
    //[fundo.layer setBorderColor:[[request corFontes] CGColor]];
    fundo.layer.borderWidth = 2;
}

- (void)puxaPedido
{
    arrayPedidoProdutos = [NSArray arrayWithArray:[conta puxaMinhaConta]];
    [btnFinalizar setEnabled:([[conta conta] count] > 0)];
    
    [total setText:[[NSString stringWithFormat:@"%@ %@ %.2f", [Language get:@"TOTAL"], [request moeda], [conta totalConta]] stringByReplacingOccurrencesOfString:@"." withString:[request separadorMoeda]]];
    
    [loading stopAnimating];
    [tabela reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performSelectorInBackground:@selector(puxaPedido) withObject:nil];
    
    [self ajustaPersonalizacao];
    
    [btnVoltar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:20]];
    [btnVoltar setTitleEdgeInsets:UIEdgeInsetsMake(6, 25, 0, 0)];
    [btnVoltar setTitle:[Language get:@"VOLTARM"] forState:UIControlStateNormal];
    
    [btnFinalizar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:20]];
    [btnFinalizar setTitleEdgeInsets:UIEdgeInsetsMake(6, -25, 0, 0)];
    [btnFinalizar setTitle:[Language get:@"FINALIZAR"] forState:UIControlStateNormal];
    [btnFinalizar setEnabled:([[conta conta] count] > 0)];
    
    //[tabela setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [titulo setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:25]];
    [titulo setText:[Language get:@"MINHA_CONTA"]];
    
    [mesa setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:25]];
    [mesa setText:[NSString stringWithFormat:@"%@ %@ %@", [Language get:@"MESA"], [Language get:@"NUM"], [conta mesa]]];
    
    [total setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:23]];
    [total setText:[[NSString stringWithFormat:@"%@ %@ 0,00", [Language get:@"TOTAL"], [request moeda]] stringByReplacingOccurrencesOfString:@"." withString:[request separadorMoeda]]];
    
    [aviso setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:13]];
    [aviso setText:[Language get:@"MSGTAXASERVICO"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
