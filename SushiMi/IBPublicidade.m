//
//  IBPublicidade.m
//  Sophia
//
//  Created by MacCVW on 06/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "IBPublicidade.h"
#import "request.h"
#import "Language.h"
#import "TimerApp.h"

@implementation IBPublicidade

- (void)avanca
{
    int pag = scrollView.contentOffset.x / scrollView.frame.size.width;
    int newpag = (pag+1) % [[request localFotosPublicidade] count];
    [scrollView scrollRectToVisible:CGRectMake(newpag * 1024, 0, 1024, 768) animated:YES];
    [pageControl setCurrentPage:newpag];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollV
{
    int pag = scrollView.contentOffset.x / scrollView.frame.size.width;
    [pageControl setCurrentPage:pag];
    
    if (!timer)
        timer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(avanca) userInfo:nil repeats:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
}

- (void)carregaImagens
{
    int x = 0;
    for (NSString *urlImg in [request localFotosPublicidade])
    {
        NSArray *a = [urlImg componentsSeparatedByString:@"/"];
        if ([a count] > 2)
        {
            NSString *nomeImg = [a objectAtIndex:a.count-3];
            UIImageView *foto = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, 1024, 768)];
            [foto setImage:[UIImage imageWithContentsOfFile:[request pathPara:nomeImg]]];
            [scrollView addSubview:foto];
            x += 1024;
        }
    }
    
    [scrollView setContentSize:CGSizeMake(x, 768)];
    [pageControl setNumberOfPages:x/1024];
}

- (IBAction)voltar:(id)sender
{
    [(TimerApp*)[UIApplication sharedApplication] resetIdleTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [btnVoltar setBackgroundImage:[request fundoBotaoTipo:CorBotaoUp1
                                                  tamanho:CGSizeMake(btnVoltar.frame.size.width, btnVoltar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateNormal];
    [btnVoltar setBackgroundImage:[request fundoBotaoTipo:CorBotaoDown
                                                  tamanho:CGSizeMake(btnVoltar.frame.size.width, btnVoltar.frame.size.height)
                                               eBordaRaio:8]
                         forState:UIControlStateHighlighted];
    [btnVoltar.layer setBorderWidth:1];
    [btnVoltar.layer setCornerRadius:8];
    btnVoltar.layer.shouldRasterize = YES;
    btnVoltar.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    btnVoltar.imageView.layer.shouldRasterize = YES;
    btnVoltar.imageView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    UIImageView *seta1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"seta_esquerda.png"]];
    [btnVoltar addSubview:seta1];
    [seta1 setFrame:CGRectMake(8, 5.5, 25, 25)];
    
    [self carregaImagens];
    timer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(avanca) userInfo:nil repeats:YES];
    
    [btnVoltar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:20]];
    [btnVoltar setTitleEdgeInsets:UIEdgeInsetsMake(6, 25, 0, 0)];
    [btnVoltar setTitle:[Language get:@"VOLTARM"] forState:UIControlStateNormal];
    
    //NSData *d = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"neto" ofType:@"html"]];
    //UIWebView *w = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    //[w loadData:d MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
    //[self.view addSubview:w];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end
