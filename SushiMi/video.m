//
//  video.m
//  SushiMi
//
//  Created by MacCVW on 08/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import "video.h"
#import <AVFoundation/AVFoundation.h>

@implementation video

- (UIImage *)thumbnailFromVideoAtURL:(NSURL *)contentURL
{
    UIImage *theImage = nil;
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:contentURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;

    Float64 durationSeconds = CMTimeGetSeconds([asset duration]);
    CMTime time = CMTimeMake(5, durationSeconds/2);

    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    
    theImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
   
    return theImage;
}

- (UIImage*)thumbnail
{
    return [self thumbnailFromVideoAtURL:[NSURL fileURLWithPath:self.path]];
}

@end
