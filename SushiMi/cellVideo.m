//
//  cellVideo.m
//  SushiMi
//
//  Created by MacCVW on 08/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import "cellVideo.h"
#import "video.h"
#import "DiegoButton.h"
#import "request.h"
#import "IBCardapio.h"

@implementation cellVideo

- (void)playVideo:(DiegoButton*)sender
{
    if ([sender tag] == 0)
        [self.pai abreVideo:self.v1];
    else
        [self.pai abreVideo:self.v2];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        //Item 1
        self.fundo1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 169, 284, 90)];
        [self.fundo1 setImage:[request corBotao:CorFundoItem]];
        [self.contentView addSubview:self.fundo1];
        
        self.video1 = [[DiegoButton alloc] initWithFrame:CGRectMake(10, 14, 275, 172)];
        [self.video1.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.video1 setTag:0];
        [self.video1 addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.video1];
        
        self.descricao1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 194, 274, 65)];
        [self.descricao1 setBackgroundColor:[UIColor clearColor]];
        [self.descricao1 setTextColor:[UIColor whiteColor]];
        [self.descricao1 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao1 setNumberOfLines:2];
        [self.descricao1 setLineBreakMode:NSLineBreakByWordWrapping];
        [self.contentView addSubview:self.descricao1];
        
        //Item 2
        self.fundo2 = [[UIImageView alloc] initWithFrame:CGRectMake(315, 169, 284, 90)];
        [self.fundo2 setImage:[request corBotao:CorFundoItem]];
        [self.contentView addSubview:self.fundo2];
        
        self.video2 = [[DiegoButton alloc] initWithFrame:CGRectMake(315 + 10, 14, 275, 172)];
        [self.video2.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.video2 setTag:1];
        [self.video2 addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.video2];

        self.descricao2 = [[UILabel alloc] initWithFrame:CGRectMake(315 + 10, 194, 274, 65)];
        [self.descricao2 setBackgroundColor:[UIColor clearColor]];
        [self.descricao2 setTextColor:[UIColor whiteColor]];
        [self.descricao2 setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17]];
        [self.descricao2 setNumberOfLines:2];
        [self.descricao2 setLineBreakMode:NSLineBreakByWordWrapping];
        [self.contentView addSubview:self.descricao2];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.video2 setHidden:!self.v2];
    [self.fundo2 setHidden:!self.v2];
    [self.descricao2 setHidden:!self.v2];
    
    [self.video1 setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    [self.video1 setBackgroundImage:[self.v1 thumbnail] forState:UIControlStateNormal];
    [self.descricao1 setText:self.v1.descricao];
    
    if (self.v2)
    {
        [self.video2 setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
        [self.video2 setBackgroundImage:[self.v2 thumbnail] forState:UIControlStateNormal];
        [self.descricao2 setText:self.v2.descricao];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
