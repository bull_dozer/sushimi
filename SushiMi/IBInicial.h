//
//  IBInicial.h
//  Sophia
//
//  Created by MacCVW on 25/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPMoviePlayerController;

@interface IBInicial : UIViewController <UIAlertViewDelegate>
{
    IBOutlet UILabel *mesa, *tituloMesa;
    IBOutlet UIImageView *barra, *fundoCalc, *logo, *background;
    IBOutlet UIButton *btnCardapio, *btnPublicidade, *btnConta;
    IBOutlet UIActivityIndicatorView *loading;
    IBOutlet UIView *viewBandeiras;
    MPMoviePlayerController *moviePlayer;
}

- (IBAction)teclaNum:(id)sender;
- (IBAction)teclaApaga:(id)sender;
- (IBAction)teclaAvanca:(id)sender;
- (IBAction)abreCardapio:(id)sender;
- (IBAction)abrePublicidade:(id)sender;
- (IBAction)setIdioma:(id)sender;
- (IBAction)showEA:(id)sender;

@end