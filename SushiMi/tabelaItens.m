//
//  tabelaItens.m
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "tabelaItens.h"
#import "cellItem.h"
#import "cellPromo.h"
#import "cellCadastro.h"
#import "cellVideo.h"
#import "cellEscolha.h"
#import "promocao.h"
#import "item.h"
#import "IBCardapio.h"

@implementation tabelaItens

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.tipo)
    {
        case TabelaItens:
            return ceil((float) self.itens.count/2);
            break;
            
        case TabelaEscolha:
            return ceil((float) self.itens.count/2);
            break;
            
        case TabelaPromo:
            return self.itens.count;
            break;
            
        case TabelaCadastro:
            return 8;
            break;
            
        case TabelaVideos:
            return ceil((float) self.itens.count/2);
            break;
            
        default:
            return 0;
            break;
    }
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.tipo)
    {
        case TabelaItens:
            return 300; //275
            break;
            
        case TabelaEscolha:
            return 275;
            break;
            
        case TabelaPromo:
            return [[[self.itens objectAtIndex:indexPath.row] imagem] size].height + 44;
            break;
            
        case TabelaCadastro:
            return 52;
            break;
            
        case TabelaVideos:
            return 275;
            break;
            
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.pai.tItens setScrollEnabled:(self.tipo != TabelaCadastro)];
    
    if (self.tipo == TabelaItens)
    {
        static NSString *CellIdentifier = @"CellItem";
        cellItem *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[cellItem alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellItem"];
            [cell setPai:self.pai];
        }

        [cell setI1:(item*)[self.itens objectAtIndex:indexPath.row*2]];
        [cell setEsconderIcons:self.esconderIcons];

        if (1+(indexPath.row*2) < self.itens.count)
            [cell setI2:(item*)[self.itens objectAtIndex:1+(indexPath.row*2)]];
        else
            [cell setI2:nil];
        
        return cell;
    }
    
    else if (self.tipo == TabelaEscolha)
    {
        static NSString *CellIdentifier = @"CellEscolha";
        cellEscolha *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[cellEscolha alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellEscolha"];
            [cell setPai:self.pai];
        }
        
        [cell setI1:(item*)[self.itens objectAtIndex:indexPath.row*2]];
        
        if (1+(indexPath.row*2) < self.itens.count)
            [cell setI2:(item*)[self.itens objectAtIndex:1+(indexPath.row*2)]];
        else
            [cell setI2:nil];
        
        return cell;
    }
    
    else if (self.tipo == TabelaPromo)
    {
        static NSString *CellIdentifier = @"CellPromo";
        cellPromo *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[cellPromo alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellPromo"];
            [cell setPai:self.pai];
        }
        
        [cell setP:[self.itens objectAtIndex:indexPath.row]];
        
        return cell;
    }
    
    else if (self.tipo == TabelaCadastro)
    {
        static NSString *CellIdentifier = @"cellCadastro";
        cellCadastro *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[cellCadastro alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellCadastro"];
            [cell setPai:self.pai];
            [cell setTabela:tableView];
        }
        
        NSArray *arrayCadastro = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cadastro" ofType:@"plist"]];
        [cell setCampo:[arrayCadastro objectAtIndex:indexPath.row]];
        
        return cell;
    }
    
    if (self.tipo == TabelaVideos)
    {
        static NSString *CellIdentifier = @"CellVideo";
        cellVideo *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[cellVideo alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellVideo"];
            [cell setPai:self.pai];
        }
        
        [cell setV1:(video*)[self.itens objectAtIndex:indexPath.row*2]];
        
        if (1+(indexPath.row*2) < self.itens.count)
            [cell setV2:(video*)[self.itens objectAtIndex:1+(indexPath.row*2)]];
        else
            [cell setV2:nil];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
