//
//  cellVideo.h
//  SushiMi
//
//  Created by MacCVW on 08/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBCardapio, DiegoButton, video;

@interface cellVideo : UITableViewCell

@property (nonatomic,strong) DiegoButton *video1, *video2;
@property (nonatomic,strong) UIImageView *fundo1, *fundo2;
@property (nonatomic,strong) UILabel *descricao1, *descricao2;
@property (nonatomic,strong) video *v1, *v2;

@property (nonatomic,strong) IBCardapio *pai;

@end
