//
//  categoria.h
//  Agraz
//
//  Created by MacCVW on 27/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface categoria : NSObject

@property (nonatomic) int idCat;
@property (nonatomic,retain) NSString *tituloPt, *tituloEn, *tituloEs;

- (NSString*)titulo;

@end
