//
//  IBMinhaConta.h
//  Sophia
//
//  Created by MacCVW on 12/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBInicial;

@interface IBMinhaConta : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
{
    IBOutlet UIButton *btnVoltar, *btnFinalizar;
    IBOutlet UIImageView *fundo, *background;
    IBOutlet UITableView *tabela;
    IBOutlet UILabel *titulo, *total, *mesa, *aviso;
    IBOutlet UIActivityIndicatorView *loading;
    
    NSArray *arrayPedidoProdutos;
    
    UIAlertView *alertViewWaiting;
    NSTimer *timerWaiting;
}

@property (nonatomic,retain) IBInicial *pai;

- (IBAction)voltar:(id)sender;
- (IBAction)fecharConta:(id)sender;

@end
