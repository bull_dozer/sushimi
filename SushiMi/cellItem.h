//
//  cellItem.h
//  Sophia
//
//  Created by MacCVW on 26/06/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class item, IBCardapio, DiegoButton;

@interface cellItem : UITableViewCell <UIAlertViewDelegate>
{
    item *aux;
    DiegoButton *btnFace;
}

@property (nonatomic,strong) DiegoButton *foto1, *foto2, *pick1, *pick2, *star1, *star2, *face1, *face2;
@property (nonatomic,strong) UIButton *btnDescricao1, *btnDescricao2;
@property (nonatomic,strong) UIImageView *fundo1, *fundo2;
@property (nonatomic,strong) UILabel *titulo1, *descricao1, *titulo2, *descricao2;

@property (nonatomic,strong) item *i1, *i2;
@property (nonatomic,strong) IBCardapio *pai;
@property BOOL esconderIcons;

@end
