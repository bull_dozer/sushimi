//
//  subcategoria.h
//  Agraz
//
//  Created by MacCVW on 02/05/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface subcategoria : NSObject

@property (nonatomic,retain) NSString *cod, *tituloPt, *tituloEn, *tituloEs;
@property (nonatomic,retain) NSMutableArray *itens;

- (NSString*)titulo;

@end
