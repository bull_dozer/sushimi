#import "TimerApp.h"
#import "AppDelegate.h"
#import "request.h"

@implementation TimerApp

-(void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    if (!myidleTimer)
        [self resetIdleTimer];
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan)
            [self resetIdleTimer];
    }
}

-(void)resetIdleTimer
{
    if (myidleTimer)
    {
        [myidleTimer invalidate];
    }

    int timeout = kApplicationTimeoutInMinutes * 60;
    myidleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
}

-(void)idleTimerExceeded
{
    AppDelegate *d = [[UIApplication sharedApplication] delegate];
    UIApplicationState appState= [[UIApplication sharedApplication] applicationState];

    if (![request modalAberta] && appState == UIApplicationStateActive)
    {
        [d.navigationController popToRootViewControllerAnimated:YES];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification object:nil];
}

@end