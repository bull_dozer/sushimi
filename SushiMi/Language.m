//
//  Language.m
//  Sophia
//
//  Created by MacCVW on 23/07/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "Language.h"

@implementation Language

static NSBundle *bundle = nil;
static NSString *lang = nil;

+ (void)initialize
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString *current = [languages objectAtIndex:0];
    
    if (![current isEqualToString:@"en"] && ![current isEqualToString:@"es"] && ![current isEqualToString:@"pt"])
        current = @"en";
    
    [self setLanguage:current];
}

+ (void)setLanguage:(NSString *)l
{
    //NSLog(@"preferredLang: %@", l);
    lang = l;
    NSString *path = [[NSBundle mainBundle] pathForResource:l ofType:@"lproj"];
    bundle = [NSBundle bundleWithPath:path];
}

+ (NSString *)get:(NSString *)key
{
    return [bundle localizedStringForKey:key value:nil table:nil];
}

+ (NSString *)getLanguage
{
    return lang;
}

@end