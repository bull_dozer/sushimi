//
//  item.m
//  Agraz
//
//  Created by MacCVW on 10/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import "item.h"
#import "request.h"
#import "Language.h"

@implementation item
@synthesize tituloPt, tituloEn, tituloEs, cod, idProd, imagem, preco, descricaoPt, descricaoEn, descricaoEs, urlFoto, thumb;

- (UIImage*)foto
{
    /*
    if (imagem.length == 0)
    {
        NSDictionary *p = [NSDictionary dictionaryWithContentsOfFile:[DOCSFOLDER stringByAppendingString:@"/categorias/personalizacao.plist"]];
        if ([[p valueForKey:@"logo"] length] > 0)
            return [UIImage imageWithContentsOfFile:[request pathPara:[p valueForKey:@"logo"]]];
    }
     */

    return [UIImage imageWithContentsOfFile:[request pathPara:imagem]];
}

- (UIImage*)thumbnail
{
    if (!thumb)
    {
        CGSize newSize = CGSizeMake(275, 172);
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
        [[self foto] drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        thumb = newImage;
    }
    
    return thumb;
}

- (NSString*)titulo
{
    return tituloPt;
}

- (NSString*)descricao
{
    return descricaoPt;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeFloat:preco forKey:@"preco"];
    [encoder encodeInt:idProd forKey:@"idProd"];
    [encoder encodeObject:tituloPt forKey:@"tituloPt"];
    [encoder encodeObject:tituloEn forKey:@"tituloEn"];
    [encoder encodeObject:tituloEs forKey:@"tituloEs"];
    [encoder encodeObject:cod forKey:@"cod"];
    [encoder encodeObject:imagem forKey:@"imagem"];
    [encoder encodeObject:descricaoPt forKey:@"descricaoPt"];
    [encoder encodeObject:descricaoEn forKey:@"descricaoEn"];
    [encoder encodeObject:descricaoEs forKey:@"descricaoEs"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    //NSString *title = [decoder decodeObjectForKey:kTitleKey];
    //float rating = [decoder decodeFloatForKey:kRatingKey];
    //return [self initWithTitle:title rating:rating];
    
    int _idProd = [decoder decodeIntForKey:@"idProd"];
    float _preco = [decoder decodeFloatForKey:@"preco"];
    NSString *_tituloPt = [decoder decodeObjectForKey:@"tituloPt"];
    NSString *_tituloEn = [decoder decodeObjectForKey:@"tituloEn"];
    NSString *_tituloEs = [decoder decodeObjectForKey:@"tituloEs"];
    NSString *_cod = [decoder decodeObjectForKey:@"cod"];
    NSString *_imagem = [decoder decodeObjectForKey:@"imagem"];
    NSString *_descricaoPt = [decoder decodeObjectForKey:@"descricaoPt"];
    NSString *_descricaoEn = [decoder decodeObjectForKey:@"descricaoEn"];
    NSString *_descricaoEs = [decoder decodeObjectForKey:@"descricaoEs"];
    
    item *i = [[item alloc] init];
    [i setIdProd:_idProd];
    [i setPreco:_preco];
    [i setTituloPt:_tituloPt];
    [i setTituloEn:_tituloEn];
    [i setTituloEs:_tituloEs];
    [i setCod:_cod];
    [i setImagem:_imagem];
    [i setDescricaoPt:_descricaoPt];
    [i setDescricaoEn:_descricaoEn];
    [i setDescricaoEs:_descricaoEs];
    return i;
}

@end
