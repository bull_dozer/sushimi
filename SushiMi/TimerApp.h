#import <Foundation/Foundation.h>
#define kApplicationTimeoutInMinutes 2
#define kApplicationDidTimeoutNotification @"AppTimeOut"

@interface TimerApp : UIApplication
{
    NSTimer *myidleTimer;
}

- (void)resetIdleTimer;

@end