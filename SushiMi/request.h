//
//  request.h
//  Agraz
//
//  Created by MacCVW on 23/04/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#define DOCSFOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define TMPFOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"]

@class item, video;

typedef enum
{
    CorBotaoUp1 = 1,
    CorBotaoUp2,
    CorBotaoDown,
    CorBotaoSelecionado,
    CorBotaoVoltarUp,
    CorBotaoVoltarDown,
    CorFundoItem
} CorBotao;

@interface request : NSObject

+ (NSString*)idCliente;
+ (NSURL*)URLBase;
+ (UIImage*)puxaFoto:(NSString*)strUrl eNomeImg:(NSString*)nome;
+ (UIImage*)puxaFotoPublicidade:(NSString*)strUrl eNomeImg:(NSString*)nome;
+ (id)puxaPlist:(NSString*)strUrl;
+ (void)puxaTudo;
+ (void)setNavController:(UINavigationController*)n;
+ (UINavigationController*)appNavController;
+ (BOOL)fazPedidos;

+ (void)setModalAberta:(BOOL)m;
+ (BOOL)modalAberta;

# pragma mark - Sushimi

+ (BOOL)avaliarProduto:(item*)i nota:(NSUInteger)n;
+ (BOOL)enviarFormNome:(NSString*)n email:(NSString*)e profissao:(NSString*)p cidade:(NSString*)c aniversario:(NSString*)a newsletter:(BOOL)l;
+ (video*)videoPrincipal;

# pragma mark - Puxa local
+ (NSArray*)localCategoriasDoMenu:(NSUInteger)idMenu;
+ (NSArray*)localProdutosCategoria:(NSUInteger)idCat;
+ (NSArray*)localSubcategoriasDaCategoria:(NSUInteger)idCat;
+ (NSArray*)localFotosPublicidade;
+ (NSArray*)localPromocoes;
+ (NSArray*)localVideos;
+ (NSArray*)localMaisVendidos;

+ (UIColor*)corFontes;
+ (UIImage*)corBotao:(CorBotao)c;
+ (UIImage*)fundoBotaoTipo:(CorBotao)tipo tamanho:(CGSize)tam eBordaRaio:(float)radius;
+ (UIColor*)corBordaTipo:(CorBotao)tipo;
+ (NSString*)moeda;
+ (NSString*)separadorMoeda;

# pragma mark - Outros
+ (UIImage*)resize:(UIImage*)img novoTamanho:(CGSize)newSize;
+ (NSString*)pathPara:(NSString*)file;
+ (void)puxaPublicidade:(BOOL)reload;
+ (UIImage *)imageFromLayer:(CALayer *)layer;
+ (void)selecionaBotaoCategoria:(UIButton*)btn;

@end
