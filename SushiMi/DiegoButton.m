//
//  DiegoButton.m
//  SushiMi
//
//  Created by MacCVW on 28/09/12.
//
//

#import "DiegoButton.h"

@implementation DiegoButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlight
{
    if (highlight)
        [self.imageView setAlpha:.5];
    else
        [self.imageView setAlpha:1];
}

@end
