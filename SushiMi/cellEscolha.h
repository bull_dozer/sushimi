//
//  cellEscolha.h
//  SushiMi
//
//  Created by MacCVW on 11/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <Foundation/Foundation.h>

@class item, IBCardapio, DiegoButton;

@interface cellEscolha : UITableViewCell
{
    item *aux;
    DiegoButton *btnFace;
}

@property (nonatomic,strong) DiegoButton *foto1, *foto2, *fechar1, *fechar2;
@property (nonatomic,strong) UIImageView *fundo1, *fundo2;
@property (nonatomic,strong) UILabel *titulo1, *descricao1, *titulo2, *descricao2;

@property (nonatomic,strong) item *i1, *i2;
@property (nonatomic,strong) IBCardapio *pai;

@end
