//
//  video.h
//  SushiMi
//
//  Created by MacCVW on 08/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface video : NSObject

@property (nonatomic,strong) NSString *path, *descricao;
@property (nonatomic,strong) UIImage *thumb;

- (UIImage*)thumbnail;

@end
