//
//  pedido.h
//  Agraz
//
//  Created by MacCVW on 16/05/12.
//  Copyright (c) 2012 Diego Trevisan Lara (iOS) & Neto Ramalho (Web) p/ CVW Soluções Tecnológicas. All rights reserved.
//

#import <Foundation/Foundation.h>

@class item;

@interface pedido : NSObject <NSURLConnectionDelegate>

+ (void)insereItem:(item*)i;
+ (void)removeItem:(item*)i;
+ (BOOL)itemEstaNoPedido:(item*)i;
+ (void)zeraPedido;
+ (NSArray*)pedido;

@end