//
//  cellCadastro.m
//  SushiMi
//
//  Created by MacCVW on 05/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import "cellCadastro.h"
#import "request.h"
#import "IBCardapio.h"
#import "tabelaItens.h"

@implementation cellCadastro

# pragma mask - Outros

- (void)checkBtn
{
    [self.check setSelected:!self.check.selected];
}

- (void)enviarForm
{
    NSMutableDictionary *dictDados = [[NSMutableDictionary alloc] initWithCapacity:5];
    
    for (int i = 1; i <= 5; i++)
    {
        cellCadastro *cell = (cellCadastro*)[self.tabela cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell.textFieldCampo.text.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Erro" message:[NSString stringWithFormat:@"Preencha o campo %@!", cell.tituloCampo.text.lowercaseString] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return;
        }
        else
        {
            [dictDados setValue:cell.textFieldCampo.text forKey:cell.tituloCampo.text.lowercaseString];
        }
    }
    
    cellCadastro *cell = (cellCadastro*)[self.tabela cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    
    BOOL enviado = [request enviarFormNome:[dictDados valueForKey:@"nome"]
                                     email:[dictDados valueForKey:@"e-mail"]
                                 profissao:[dictDados valueForKey:@"profissão"]
                                    cidade:[dictDados valueForKey:@"cidade"]
                               aniversario:[dictDados valueForKey:@"aniversário"]
                                newsletter:[cell.check isSelected]];
    
    if (enviado)
    {
        [[[UIAlertView alloc] initWithTitle:@"Sucesso" message:@"Obrigado pelo seu cadastro!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        [self.tabela reloadData];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Erro" message:@"Ocorreu um erro. Por favor tente novamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

# pragma mask - TextField

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (self.pai.view.frame.origin.y == 0)
        [UIView animateWithDuration:0.3 animations:^
         {
             CGRect frame = self.pai.view.frame;
             frame.origin.y -= 178;
             [self.pai.view setFrame:frame];
         }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if (self.pai.view.frame.origin.y == -178)
        [UIView animateWithDuration:0.3 animations:^
         {
             CGRect frame = self.pai.view.frame;
             frame.origin.y = 0;
             [self.pai.view setFrame:frame];
         }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //if (self.pai.tabelaItensDelegate.tipo != TabelaCadastro)
        
}

# pragma mask - Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.fundo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 617, 52)];
        [self.fundo setImage:[request corBotao:CorBotaoUp2]];
        [self addSubview:self.fundo];
        
        self.tituloCampo = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 52)];
        [self.tituloCampo setBackgroundColor:[UIColor clearColor]];
        [self.tituloCampo setTextColor:[UIColor whiteColor]];
        [self.tituloCampo setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self addSubview:self.tituloCampo];
        
        self.textFieldCampo = [[UITextField alloc] initWithFrame:CGRectMake(140, 2, 450, 38)];
        [self.textFieldCampo setBackgroundColor:[UIColor colorWithPatternImage:[request corBotao:CorFundoItem]]];
        [self.textFieldCampo.layer setBorderWidth:2];
        [self.textFieldCampo.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        [self.textFieldCampo setTextColor:[UIColor whiteColor]];
        [self.textFieldCampo setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:22]];
        [self.textFieldCampo setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [self.textFieldCampo setDelegate:self];
        [self addSubview:self.textFieldCampo];
        
        self.check = [[UIButton alloc] initWithFrame:CGRectMake(26, 13, 25, 25)];
        [self.check setImage:[UIImage imageNamed:@"checkoncad.png"] forState:UIControlStateSelected];
        [self.check setImage:[UIImage imageNamed:@"checkoffcad.png"] forState:UIControlStateNormal];
        [self.check addTarget:self action:@selector(checkBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.check];
        
        self.tituloCheck = [[UILabel alloc] initWithFrame:CGRectMake(65, 4, 500, 52)];
        [self.tituloCheck setBackgroundColor:[UIColor clearColor]];
        [self.tituloCheck setTextColor:[UIColor whiteColor]];
        [self.tituloCheck setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
        [self.tituloCheck setText:@"Desejo receber novidades e promoções."];
        [self addSubview:self.tituloCheck];
        
        self.enviar = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.enviar setFrame:CGRectMake(520, 5, 80, 52)];
        [self.enviar.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-BoldCond" size:26]];
        [self.enviar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.enviar setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [self.enviar setTitle:@"ENVIAR" forState:UIControlStateNormal];
        [self.enviar addTarget:self action:@selector(enviarForm) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.enviar];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.textFieldCampo setText:@""];
    switch ([[self.campo valueForKey:@"tipo"] intValue])
    {
        case 0:
            [self.tituloCampo setHidden:YES];
            [self.textFieldCampo setHidden:YES];
            [self.tituloCheck setHidden:YES];
            [self.check setHidden:YES];
            [self.enviar setHidden:YES];
            break;
            
        case 1:
            [self.tituloCampo setHidden:NO];
            [self.textFieldCampo setHidden:NO];
            [self.tituloCheck setHidden:YES];
            [self.check setHidden:YES];
            [self.enviar setHidden:YES];
            
            [self.tituloCampo setText:[self.campo valueForKey:@"nome"]];
            break;
            
        case 2:
            [self.tituloCampo setHidden:YES];
            [self.textFieldCampo setHidden:YES];
            [self.tituloCheck setHidden:NO];
            [self.check setHidden:NO];
            [self.enviar setHidden:YES];
            break;
            
        case 3:
            [self.tituloCampo setHidden:YES];
            [self.textFieldCampo setHidden:YES];
            [self.tituloCheck setHidden:YES];
            [self.check setHidden:YES];
            [self.enviar setHidden:NO];
            break;
            
        default:
            [self.tituloCampo setHidden:YES];
            [self.textFieldCampo setHidden:YES];
            [self.tituloCheck setHidden:YES];
            [self.check setHidden:YES];
            [self.enviar setHidden:YES];
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end