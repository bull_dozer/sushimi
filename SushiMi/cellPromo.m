//
//  cellPromo.m
//  SushiMi
//
//  Created by MacCVW on 04/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import "cellPromo.h"
#import "DiegoButton.h"
#import "promocao.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation cellPromo

- (void)publicarNoFacebookMensagem:(NSString*)msg
{
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [act setFrame:self.face.frame];
    [self.face setAlpha:0.5];
    [self addSubview:act];
    [act startAnimating];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    if (msg)
        [param setValue:msg forKey:@"message"];
    [param setValue:@"http://www.sushimi.com.br/" forKey:@"link"];
    [param setValue:self.p.url forKey:@"picture"];
    //[param setValue:aux.titulo forKey:@"name"];
    //[param setValue:aux.descricao forKey:@"description"];
    
    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:param
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (error)
         {
             NSString *alertText = [NSString stringWithFormat: @"Ocorreu um erro: domain = %@, code = %d", error.domain, error.code];
             [[[UIAlertView alloc] initWithTitle:@"Erro" message:alertText delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil] show];
         }
         else
         {
             //[[[UIAlertView alloc] initWithTitle:@"Publicado!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
         }
         
         [act stopAnimating];
         [act removeFromSuperview];
         [self.face setAlpha:1];
     }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)     //inserir recado na publicacao?
    {
        if (buttonIndex == 1)   //sim
        {
            UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Recado" message:nil delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Publicar", nil];
            [a setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [[a textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
            [a setTag:2];
            [a show];
        }
        else                    //nao
        {
            [self publicarNoFacebookMensagem:nil];
        }
    }
    
    else if (alertView.tag == 2) //insere recado na publicacao
    {
        if (buttonIndex == 1)
        {
            [self publicarNoFacebookMensagem:[[alertView textFieldAtIndex:0] text]];
        }
    }
}

- (void)facebook:(DiegoButton*)sender
{
    NSLog(@"%@", self.p.url);
    if (FBSession.activeSession.isOpen)
    {
        UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Mensagem" message:@"Deseja inserir um recado junto da publicação?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil];
        [a setTag:1];
        [a show];
    }
    else
    {
        FBSession.activeSession = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObject:@"publish_actions"]];
        [[FBSession activeSession] openWithBehavior:FBSessionLoginBehaviorForcingWebView completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             if (error)
             {
             }
         }];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.imgViewPromo = [[UIImageView alloc] init];
        [self addSubview:self.imgViewPromo];
        
        self.face = [[DiegoButton alloc] init];
        [self.face setImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
        [self.face addTarget:self action:@selector(facebook:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.face];
    }
    return self;
}

- (void)layoutSubviews
{
    [self.imgViewPromo setImage:self.p.imagem];
    [self.imgViewPromo setFrame:CGRectMake(308 - (self.p.imagem.size.width/2), 14, self.p.imagem.size.width, self.p.imagem.size.height)];
    [self.imgViewPromo sizeToFit];
    
    [self.face setFrame:CGRectMake(308 + (self.p.imagem.size.width/2) - 13, 1, 25, 25)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
