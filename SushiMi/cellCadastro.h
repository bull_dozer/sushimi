//
//  cellCadastro.h
//  SushiMi
//
//  Created by MacCVW on 05/10/12.
//  Copyright (c) 2012 Diego Trevisan Lara (CVW Soluções Tecnológicas). All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBCardapio;

@interface cellCadastro : UITableViewCell <UITextFieldDelegate>

@property (nonatomic,strong) IBCardapio *pai;
@property (nonatomic,strong) UITableView *tabela;
@property (nonatomic,strong) UIImageView *fundo;
@property (nonatomic,strong) UILabel *tituloCampo, *tituloCheck;
@property (nonatomic,strong) UITextField *textFieldCampo;
@property (nonatomic,strong) UIButton *check, *enviar;

@property (nonatomic,strong) NSDictionary *campo;

@end
